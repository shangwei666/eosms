<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<div class="admin-sidebar am-offcanvas" id="admin-offcanvas">
  <div class="am-offcanvas-bar admin-offcanvas-bar">
    <ul class="am-list admin-sidebar-list">
      <li><a href="index.do"><span class="am-icon-home"></span> 首页</a></li>
      <li class="admin-parent">
        <a class="am-cf" data-am-collapse="{target: '#collapse-nav'}"><span class="am-icon-building"></span> 项目管理<span class="am-icon-angle-right am-fr am-margin-right"></span></a>
        <ul class="am-list am-collapse admin-sidebar-sub am-in" id="collapse-nav">
          <li><a href="show_add_project.do" class="am-cf"><span class="am-icon-file-o"></span> 新建向导<span class="am-icon-star am-fr am-margin-right admin-icon-yellow"></span></a></li>
          <li><a href="show_project.do"><span class="am-icon-table"></span> 项目查询<span class="am-badge am-badge-secondary am-margin-right am-fr">${porjct_total}</span></a></li>
        </ul>
      </li>
      <li><a href="sale_manage_region.do"><span class="am-icon-table"></span> 销控表</a></li>
      <li><a href="show_client.do"><span class="am-icon-users"></span> 客户服务</a></li>
      <li><a href="report_main.do"><span class="am-icon-pencil-square-o"></span> 业绩报表</a></li>
      <li class="admin-parent">
        <a class="am-cf" data-am-collapse="{target: '#collapse-nav-manage'}"><span class="am-icon-bars"></span> 系统管理<span class="am-icon-angle-right am-fr am-margin-right"></span></a>
        <ul class="am-list am-collapse admin-sidebar-sub am-in" id="collapse-nav-manage">
          <li><a href="show_add_company.do" class="am-cf"><span class="am-icon-bank"></span> 机构管理</a></li>
          <li><a href="show_add_menu.do" class="am-cf"><span class="am-icon-sitemap"></span> 菜单管理</a></li>
          <li><a href="show_add_role.do" class="am-cf"><span class="am-icon-check"></span> 角色管理</a></li>
          <li><a href="show_add_user.do" class="am-cf"><span class="am-icon-user"></span> 人员管理</a></li>
        </ul>
      </li>
      <li><a href="#"><span class="am-icon-sign-out"></span> 注销</a></li>
    </ul>

    <div class="am-panel am-panel-default admin-sidebar-panel">
      <div class="am-panel-bd">
        <p><span class="am-icon-bookmark"></span> 公告</p>
        <p>我本楚狂人，凤歌笑孔丘。手持绿玉杖，朝别黄鹤楼。—— IFS</p>
      </div>
    </div>
  </div>
</div>