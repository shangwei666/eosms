<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!doctype html>
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>IFS管家</title>
  <meta name="description" content="IFS管家">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="IFS管家" />
  <link rel="stylesheet" href="assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="assets/css/admin.css">
</head>
<body>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<jsp:include page="/header.do" flush="true"/>
<div class="am-cf admin-main">
  <!-- sidebar start -->
  <jsp:include page="/tree.do" flush="true"/>
  <!-- sidebar end -->

<!-- content start -->
<div class="admin-content">

    <div class="am-cf am-padding">
      <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">销控表</strong> / <small>sales manage</small></div>
    </div>

    <div class="am-g">

    </div>

    <div class="am-g">
      <div class="am-u-sm-12">
        <form class="am-form">
          <table class="am-table am-table-bordered">
            <thead>
              <tr>
              	<th colspan="${floors.get(0).size()+1}"><a href="/eosms/sale_manage_region.do?project_id=${project.id}">${project.name}</a>--<a href="/eosms/sale_manage_building.do?region_id=${region.id}">${region.name}</a>--${building.building_num}号楼</th>
              </tr>
              <tr>
                <th class="table-set" rowspan="2"  width="10%">楼层</th>
                <c:forEach  var="unit" items="${units}">
                <th class="table-set" colspan="${unit_ua_map.get(unit.id).size()}">${unit.unit_num}</th>
                </c:forEach>
              </tr>
              <tr>
                <c:forEach  var="shape" items="${shapes}">
                <th class="table-set">${shape}</th>
                </c:forEach>
              </tr>
          </thead>
          <tbody>
          	<c:forEach var="floor" items="${floors}" varStatus="s">
            <tr>
              <td>${floors.size()-s.index}</td>
              <c:forEach var="apartment" items="${floor}">
              <td><a href="show_apartment.do?apartment_id=${unit_room_map.get(apartment).id}">${unit_room_map.get(apartment).name}</a></td>
              </c:forEach>
            </tr>
            </c:forEach>
          </tbody>
        </table>
          <hr />
          <p>注：图例</p>
        </form>
      </div>

    </div>
  </div>
<!-- content end -->

</div>

<a href="#" class="am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}">
  <span class="am-icon-btn am-icon-th-list"></span>
</a>

<jsp:include page="/foot.do" flush="true"/>

<!--[if lt IE 9]>
<script src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="assets/js/amazeui.ie8polyfill.min.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="assets/js/jquery.min.js"></script>
<!--<![endif]-->
<script src="assets/js/amazeui.min.js"></script>
<script src="assets/js/app.js"></script>
<script type="text/javascript">
function sale_manage_apartment(id){
	window.location.href="/eosms/sale_manage_apartment.do?building_id="+id; 
}
</script>
</body>
</html>
