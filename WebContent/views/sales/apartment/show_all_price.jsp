<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!doctype html>
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>IFS管家</title>
  <meta name="description" content="IFS管家">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="IFS管家" />
  <link rel="stylesheet" href="assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="assets/css/admin.css">
</head>
<body>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<jsp:include page="/header.do" flush="true"/>
<div class="am-cf admin-main">
  <!-- sidebar start -->
  <jsp:include page="/tree.do" flush="true"/>
  <!-- sidebar end -->

<!-- content start -->
<div class="admin-content">

    <div class="am-cf am-padding">
      <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">${project.name}--${region.name}--${building.building_num}号楼--${unit.unit_num}单元--${apartment.name}室</strong> / <small>price</small></div>
    </div>

    <div class="am-g">

    </div>

    <div class="am-g">
      <div class="am-u-sm-12">
        <form class="am-form">
          <table class="am-table am-table-striped am-table-hover table-main">
            <thead>
              <tr>
                <th class="table-check" width="3%"><input type="checkbox" /></th>
                <th class="table-id" width="20%">单价</th>
                <th class="table-id" width="20%">总价</th>
                <th class="table-id" width="15%">导入时间</th>
                <th class="table-id" width="15%">执行时间</th>
                <th class="table-id" width="10%">状态</th>
                <th class="table-set" width="10%">操作</th>
              </tr>
          </thead>
          <tbody>
          <c:forEach var="price" items="${price_list}" varStatus="s">
            <tr>
              <td><input type="checkbox"></td>
              <td><c:out value="${price.unit_price}"/></td>
              <td><c:out value="${price.total_price}"/></td>
              <td><fmt:formatDate value="${price.create_date}" type="date" dateStyle="default"/></td>
              <td id="execute_${price.id}"><fmt:formatDate value="${price.execute_date}" type="date" dateStyle="default"/></td>
              <td id="status_${price.id}">
              	<c:if test="${price.status == 0}">停用</c:if>
              	<c:if test="${price.status == 1}">通过</c:if>
              	<c:if test="${price.status == 2}">待审批</c:if>
              </td>
              <td>
                <div class="am-btn-toolbar">
                  <div id="approve_${price.id}" class="am-btn-group am-btn-group-xs">
                  <c:if test="${price.status == 2}">
                    <button type="button" class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="approve(${price.id})"><span class="am-icon-pencil-square-o"></span> 同意</button>
                  </c:if>
                  </div>
                </div>
              </td>
            </tr>
            </c:forEach>
          </tbody>
        </table>
          <hr />
          <p>注：只能对待审核价格操作  </p> 
          <hr />
          <a class="am-btn am-btn-link" href="<%=request.getContextPath()%>/show_apartment.do?apartment_id=${apartment.id}">返回</a>
        </form>
      </div>

    </div>
  </div>
<!-- content end -->

</div>

<a href="#" class="am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}">
  <span class="am-icon-btn am-icon-th-list"></span>
</a>

<jsp:include page="/foot.do" flush="true"/>

<!--[if lt IE 9]>
<script src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="assets/js/amazeui.ie8polyfill.min.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="assets/js/jquery.min.js"></script>
<!--<![endif]-->
<script src="assets/js/amazeui.min.js"></script>
<script src="assets/js/app.js"></script>
<script type="text/javascript">
function approve(id){
	$.ajax({
		contentType:"application/json",
	    type : "GET",
	    url : "<%=request.getContextPath()%>/approve_price.do?id="+id,
	    dataType: "json",
	    success : function(data) {
	        if(data.flag=="1"){
	            alert("审批通过！");
	            var mydate = new Date();
	            var str = "" + mydate.getFullYear() + "-";
	            str += (mydate.getMonth()+1) + "-";
	            str += mydate.getDate() + "-";
	            $("#execute_"+id).html(str);
	            $("#status_"+id).html("通过");
	            $("#approve_"+id).html("");
	        }
	        else{
	            alert("审批失败！");
	        }
	    },
	    error :function(){
	        alert("网络连接出错！");
	    }
	});
}
</script>
</body>
</html>
