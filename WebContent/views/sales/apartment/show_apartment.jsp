<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!doctype html>
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>IFS管家</title>
  <meta name="description" content="IFS管家">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="IFS管家" />
  <link rel="stylesheet" href="assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="assets/css/admin.css">
</head>
<body>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<jsp:include page="/header.do" flush="true"/>

<div class="am-cf admin-main">
  <!-- sidebar start -->
  <jsp:include page="/tree.do" flush="true"/>
  <!-- sidebar end -->

<!-- content start -->
<form id="apartment_form" class="am-form" action="save_apartment.do" method="post">
<input type="hidden" name="id" id="id" value="${apartment.id}">
<div class="admin-content">

  <div class="am-cf am-padding">
    <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">房屋</strong> / <small>room</small></div>
  </div>
  <div class="am-tabs am-margin" data-am-tabs>
    <ul class="am-tabs-nav am-nav am-nav-tabs">
      <li class="am-active"><a href="#tab1">基本信息</a></li>
    </ul>


    <div class="am-tabs-bd">
      <div class="am-tab-panel am-fade am-in am-active" id="tab1">
           <div class="am-g am-margin-top">
            <div class="am-u-sm-4 am-u-md-2 am-text-right">
              房间
            </div>
            <div class="am-u-sm-8 am-u-md-4">
              <input type="text" class="am-input-sm" value="${project.name}--${region.name}--${building.building_num}号楼--${unit.unit_num}单元--${apartment.name}室" readonly="readonly">
            </div>
            <div class="am-u-sm-4 am-u-md-2 am-text-right">
              销控状态
            </div>
            <div class="am-u-sm-8 am-u-md-4">
            <select name="sale_status" id="sale_status">
            	<c:forEach  var="sale_status" items="${params_sale_status}" varStatus="s">
            	<option value="${sale_status.para_value}" <c:if test="${sale_status.para_value==apartment.sale_status}">selected</c:if>>${sale_status.para_name}</option>
            	</c:forEach>
            </select>
            </div>
          </div>
          
		  <div class="am-g am-margin-top">
            <div class="am-u-sm-4 am-u-md-2 am-text-right">
              建筑类型
            </div>
            <div class="am-u-sm-8 am-u-md-4">
	        	<select readonly="readonly">
	        		<c:forEach  var="buildtype" items="${params_buildtype}" varStatus="s">
	        		<option value="${buildtype.para_value}" <c:if test="${buildtype.para_value==apartmentShape.build_type}">selected</c:if>>${buildtype.para_name}</option>
	        		</c:forEach>
	        	</select>
            </div>
            <div class="am-u-sm-4 am-u-md-2 am-text-right">
              装修标准
            </div>
            <div class="am-u-sm-8 am-u-md-4">
            	<select readonly="readonly">
	            	<c:forEach  var="finish" items="${params_finish}" varStatus="s">
	            	<option value="${finish.para_value}" <c:if test="${finish.para_value==apartmentShape.decoration}">selected</c:if>>${finish.para_name}</option>
	            	</c:forEach>
              	</select>
            </div>
          </div>
          
          <div class="am-g am-margin-top">
            <div class="am-u-sm-2 am-u-md-2 am-text-right">
              户型编号
            </div>
            <div class="am-u-sm-4 am-u-md-4">
              <input type="text" class="am-input-sm" value="${apartmentShape.code}" readonly="readonly">
            </div>
            <div class="am-u-sm-2 am-u-md-2 am-text-right">
              户型结构
            </div>
            <div class="am-u-sm-4 am-u-md-4">
              <input type="text" class="am-input-sm" value="${apartmentShape.unit_type}" readonly="readonly">
            </div>
          </div>
          
          <div class="am-g am-margin-top">
            <div class="am-u-sm-4 am-u-md-2 am-text-right">
              建筑面积
            </div>
            <div class="am-u-sm-8 am-u-md-2">
              <input type="number" id="build_space" name="build_space" placeholder="输入面积" class="am-input-sm" size="40" value="${apartment.build_space}" required>
            </div>
            <div class="am-hide-sm-only am-u-md-2 am-text-right">平方米</div>
            <div class="am-u-sm-4 am-u-md-2 am-text-right">
              套内面积
            </div>
            <div class="am-u-sm-8 am-u-md-2">
              <input type="number" id="inside_space" name="inside_space" placeholder="输入面积" class="am-input-sm" value="${apartment.inside_space}" required>
            </div>
            <div class="am-hide-sm-only am-u-md-2 am-text-right">平方米</div>
          </div>
          
          <div class="am-g am-margin-top">
            <div class="am-u-sm-2 am-u-md-2 am-text-right">
              单价
            </div>
            <div class="am-u-sm-3 am-u-md-2">
              <input type="number" class="am-input-md" placeholder="输入价格" id="unit_price" name="unit_price" value="${apartment.unit_price}" size="15" <c:if test="${price_reveiwing=='1'}">readonly="readonly"</c:if> required >
            </div>
            <c:if test="${price_reveiwing=='1'}">
            <div id="unit_price_div" class="am-hide-sm-only am-u-md-2 am-text-right am-text-danger">元/㎡(待审批)</div>
            </c:if>
            <c:if test="${price_reveiwing=='0'}">
            <div id="unit_price_div"class="am-hide-sm-only am-u-md-2 am-text-right">元/㎡</div>
            </c:if>
            <div class="am-u-sm-2 am-u-md-2 am-text-right">
              总价
            </div>
            <div class="am-u-sm-3 am-u-md-2">
              <input type="number" placeholder="输入价格" id="total_price" name="total_price" value="${apartment.total_price}" <c:if test="${price_reveiwing=='1'}">readonly="readonly"</c:if> required>
            </div>
            <c:if test="${price_reveiwing=='1'}">
            <div id="total_price_div" class="am-hide-sm-only am-u-md-2 am-text-right am-text-danger">元/㎡(待审批)</div>
            </c:if>
            <c:if test="${price_reveiwing=='0'}">
            <div id="total_price_div" class="am-hide-sm-only am-u-md-2 am-text-right">元/㎡</div>
            </c:if>
          </div>
          
          <div class="am-g am-margin-top">
            <div class="am-u-sm-4 am-u-md-2 am-text-right">
              是否有样板间
            </div>
            <div class="am-u-sm-8 am-u-md-4">
              <select name="isHaveModel">
              	<option value="1" <c:if test="${apartment.isHaveModel==1}">selected</c:if>>是</option>
              	<option value="0" <c:if test="${apartment.isHaveModel==0}">selected</c:if>>否</option>
              </select>
            </div>
            <div class="am-u-sm-12 am-u-md-6"></div>
      	   </div>
      	   <div class="am-g am-margin-top" id="client_div">
            <div class="am-u-sm-2 am-u-md-2 am-text-right">
             	客户电话
            </div>
            <div class="am-u-sm-3 am-u-md-2">
              <input type="number" class="am-input-md" placeholder="输入手机号码" id="phone" size="11" value="${client.phone}">
              <input type="hidden" class="am-input-md" name="client_id" id="client_id" value="${client.id}">
            </div>
            <div id="unit_price_div" class="am-hide-sm-only am-u-md-2 am-text-right">手机全号码</div>
            <div class="am-u-sm-2 am-u-md-2 am-text-right">
              	客户名称
            </div>
            <div class="am-u-sm-3 am-u-md-2">
              <input type="text" placeholder="系统自动匹配" id="client_name" readonly="readonly" value="${client.name}">
            </div>
            <div id="total_price_div" class="am-hide-sm-only am-u-md-2 am-text-right am-text-danger"></div>
          </div>
    </div>
  </div>
 </div>
  <div class="am-margin">
    <button type="button" id="save_apartment" class="am-btn am-btn-primary am-btn-xs">保存基本信息</button>
    <button type="button" id="save_price" class="am-btn am-btn-primary am-btn-xs">保存价格</button>
    <button type="button" class="am-btn am-btn-warning am-btn-xs" onclick="all_price()"><i class="am-icon-shopping-cart"></i>查看历史价格</button>
    <button type="button" class="am-btn am-btn-danger am-btn-xs" onclick="do_back()">返回</button>
    <button type="button" id="prints_button" class="am-btn am-btn-danger am-btn-xs">打印</button>
  </div>
  <div class="am-modal am-modal-alert" tabindex="-1" id="base-alert">
	  <div class="am-modal-dialog">
	    <div class="am-modal-hd">IFS Manage</div>
	    <div class="am-modal-bd">
	      保存基本信息，价格将不改变
	    </div>
	    <div class="am-modal-footer">
	      <span class="am-modal-btn" onclick="do_submit()">确定</span>
	    </div>
	  </div>
  </div>
  <div class="am-modal am-modal-alert" tabindex="-1" data-am-alert>
	  <div class="am-modal-dialog">
	    <div class="am-modal-hd">IFS Manage</div>
	    <div class="am-modal-bd">
	      保存价格，原始价格将成为历史
	    </div>
	    <div class="am-modal-footer">
	      <span class="am-modal-btn">确定</span>
	    </div>
	  </div>
  </div>
</div>
</form>
<!-- content end -->

</div>

<a href="#" class="am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}">
  <span class="am-icon-btn am-icon-th-list"></span>
</a>

<jsp:include page="/foot.do" flush="true"/>

<!--[if lt IE 9]>
<script src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="assets/js/amazeui.ie8polyfill.min.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="assets/js/jquery.min.js"></script>
<!--<![endif]-->
<script src="assets/js/amazeui.min.js"></script>
<script src="assets/js/app.js"></script>
<script type="text/javascript">
$(function() {	  
	  $("#save_apartment").click(function() {
			$.ajax({
				contentType:"application/json",
			    type : "GET",
			    url : "<%=request.getContextPath()%>/check_price.do?apartment_id="+$("#id").val(),
			    dataType: "json",   
			    success : function(data) {
			        if(data.flag=="0"){
			            alert("存在待审批金额，不能修改房屋状态");
			        }
			        else{
			        	if($("#sale_status").val()>2){
			        		if($("#unit_price").val()==""){
			        			alert("没有房屋价格，不能更新成出售状态");
			        			return;
			        		}
			        		if($("#client_id").val()==""){
			        			alert("没有正确客户信息，不能更新成出售状态");
			        			return;
			        		}
			        		var saveData={"id":$("#id").val(),"build_space":$("#build_space").val(),"inside_space":$("#inside_space").val(),"sale_status":$("#sale_status").val(),"client_id":$("#client_id").val()};
			        	}
			        	else{
			        		var saveData={"id":$("#id").val(),"build_space":$("#build_space").val(),"inside_space":$("#inside_space").val(),"sale_status":$("#sale_status").val(),"client_id":0};
			        	}
			        	$.ajax({
							contentType:"application/json",
						    type : "POST",
						    url : "<%=request.getContextPath()%>/save_apartment.do",
						    data : JSON.stringify(saveData),
						    dataType: "json",   
						    success : function(data) {
						        if(data.flag=="1"){
						            alert("保存成功");
						        }
						        else if(data.flag == "2"){
						        	alert("存在待审批金额，不能修改房屋状态");
						        }
						        else{
						            alert("保存失败");
						        }
						    },
						    error :function(){
						        alert("网络连接出错！");
						    }
						});
			        }
			    },
			    error :function(){
			        alert("网络连接出错！");
			    }
			});
		});
	  
	  $("#save_price").click(function() {
			$.ajax({
				contentType:"application/json",
			    type : "GET",
			    url : "<%=request.getContextPath()%>/check_price.do?apartment_id="+$("#id").val(),
			    dataType: "json",   
			    success : function(data) {
			        if(data.flag=="0"){
			            alert("存在待审批金额，不能保存");
			        }
			        else{
			        	var saveData={"id":$("#id").val(),"unit_price":$("#unit_price").val(),"total_price":$("#total_price").val()};
			        	$.ajax({
							contentType:"application/json",
						    type : "POST",
						    url : "<%=request.getContextPath()%>/save_price.do",
						    data : JSON.stringify(saveData),
						    dataType: "json",   
						    success : function(data) {
						        if(data.flag=="1"){
						            alert("保存成功");
						            $("#unit_price").attr("readonly","readonly");
						            $("#total_price").attr("readonly","readonly");
						            $("#unit_price_div").html("元/㎡(待审批)");
						            $("#total_price_div").html("元/㎡(待审批)");
						            $("#unit_price_div").removeClass().addClass("am-hide-sm-only am-u-md-2 am-text-right am-text-danger");
						            $("#total_price_div").removeClass().addClass("am-hide-sm-only am-u-md-2 am-text-right am-text-danger");
						        }
						        else if(data.flag == "2"){
						        	alert("存在待审批金额，不能保存");
						        }
						        else{
						            alert("保存失败");
						        }
						    },
						    error :function(){
						        alert("网络连接出错！");
						    }
						});
			        }
			    },
			    error :function(){
			        alert("网络连接出错！");
			    }
			});
		});
	  
	  $("#prints_button").click(function(){
		  try{ 
			    var LODOP=getLodop(); 
				if (LODOP.VERSION) {
					 if (LODOP.CVERSION)
					 alert("当前有C-Lodop云打印可用!\n C-Lodop版本:"+LODOP.CVERSION+"(内含Lodop"+LODOP.VERSION+")"); 
					 else
					 alert("本机已成功安装了Lodop控件！\n 版本号:"+LODOP.VERSION); 
				};
				
				LODOP.PRINT_INIT("打印控件功能演示_Lodop功能_表单一");
				LODOP.SET_PRINT_STYLE("FontSize",18);
				LODOP.SET_PRINT_STYLE("Bold",1);
				LODOP.ADD_PRINT_TEXT(50,231,260,39,"打印页面部分内容");
				LODOP.ADD_PRINT_HTM(88,200,350,600,document.getElementById("apartment_form").innerHTML);
				LODOP.PREVIEW();

			 }catch(err){ 
		     }
	  });
	  
	//电话号码失去焦点事件
		$('#phone').blur(function() {
			 var show_phone = this.value;
			 if(show_phone.length != 11){
				 alert("请输入正确手机号码");
			 }
			 else{
				 var r,re;
			     re = /^[1][345678]\d{9}$/; //\d表示数字,*表示匹配多个数字
			     r = show_phone.match(re);
			     if(r!=show_phone){
			    	 alert("请输入正确手机号码");
			     }
			     else{
			    	 $("#client_id").val("");
			    	 $("#client_name").val("");
			    	 $.ajax({
							contentType:"application/json",
						    type : "GET",
						    url : "<%=request.getContextPath()%>/search_client.do?phone="+show_phone,
						    dataType: "json",   
						    success : function(data) {
						        if(data.flag=="1"){
						        	alert(data.client.id);
						        	$("#client_id").val(data.client.id);
						        	$("#client_name").val(data.client.name);
						        }
						        else{
						            alert("电话号码错误");
						        }
						    },
						    error :function(){
						        alert("网络连接出错！");
						    }
						});
			     }
			 }
		 });
	
		$('#sale_status').blur(function() {
			if($('#sale_status').val()>2){
				$('#client_div').show();
			}
			else{
				$('#client_div').hide();
			}
		});
});

function do_submit(){
	$("#apartment_form").submit();
}

function do_back(){
	window.location.href="sale_manage_apartment.do?building_id=${building.id}";
}

function all_price(){
	window.location.href="show_all_price.do?apartment_id=${apartment.id}";
}
</script>
<script language="javascript" src="prints/LodopFuncs.js"></script>
<object  id="LODOP_OB" classid="clsid:2105C259-1E0C-4534-8141-A753534CB4CA" width=0 height=0> 
       <embed id="LODOP_EM" type="application/x-print-lodop" width=0 height=0></embed>
</object>
</body>
</html>
