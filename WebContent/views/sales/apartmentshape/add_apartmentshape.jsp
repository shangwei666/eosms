<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!doctype html>
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>IFS管家</title>
  <meta name="description" content="IFS管家">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="IFS管家" />
  <link rel="stylesheet" href="assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="assets/css/admin.css">
</head>
<body>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<jsp:include page="/header.do" flush="true"/>

<div class="am-cf admin-main">
  <!-- sidebar start -->
  <jsp:include page="/tree.do" flush="true"/>
  <!-- sidebar end -->

<!-- content start -->
<div class="admin-content">

    <div class="am-cf am-padding">
      <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">户型</strong> / <small>ApartmentShape</small></div>
    </div>

    <div class="am-g">
      <div class="am-u-sm-12 am-u-md-6">
        <div class="am-btn-toolbar">
          <div class="am-btn-group am-btn-group-xs">
            <button type="button" id="add_button" class="am-btn am-btn-default"><span class="am-icon-plus"></span> 新增</button>
            <button type="button" id="save_button" class="am-btn am-btn-default"><span class="am-icon-save"></span> 保存</button>
            <button type="button" id="delete_button" class="am-btn am-btn-default"><span class="am-icon-trash-o"></span> 删除</button>
            <button type="button" id="return_button" class="am-btn am-btn-default"><span class="am-icon-reply"></span> 项目列表</button>
          </div>
        </div>
      </div>
      <div class="am-u-sm-12 am-u-md-3">
        <div class="am-form-group">
        &nbsp;
        </div>
      </div>
      <div class="am-u-sm-12 am-u-md-3">
		<div class="am-form-group">
        &nbsp;
        </div>
      </div>
    </div>

    <div class="am-g">
      <div class="am-u-sm-12">
        <form class="am-form">
        <input type="hidden" id="project_id" value="${project_id}">
          <table class="am-table am-table-bordered">
            <thead>
              <tr>
                <th class="table-check"></th>
                <th class="table-title" width="15%">户型编号</th>
                <th class="table-title" width="15%">户型结构</th>
                <th class="table-title" width="15%">建筑类型</th>
                <th class="table-title" width="15%">装修标准</th>
                <th class="table-title" width="15%">建筑面积</th>
                <th class="table-title" width="15%">套内面积</th>
                <th class="table-title" width="10%">状态</th>
              </tr>
          	</thead>
          	<tbody>
          	<c:forEach var="apartmentShape" items="${apartmentShapes}" varStatus="s">
              <tr>
              	<td><input type="radio" name="apartmentShape_index" value="${s.index}">
              		<input type="hidden" id="id_${s.index}" value="${apartmentShape.id}">
              	</td>
              	<td><input type="text" id="code_${s.index}" value="${apartmentShape.code}"></td>
              	<td><input type="text" id="unit_type_${s.index}" value="${apartmentShape.unit_type}"></td>
              	<td>
              		<select id="build_type_${s.index}">
              			<c:forEach var="params_buildtype" items="${params_buildtypes}">
              				<option value="${params_buildtype.id}" <c:if test="${apartmentShape.build_type == params_buildtype.id}">selected</c:if>>${params_buildtype.para_name}</option>
              			</c:forEach>
              		</select>
              	</td>
              	<td>
              		<select id="params_finish_${s.index}">
              			<c:forEach var="params_finish" items="${params_finishs}">
              				<option value="${params_finish.id}" <c:if test="${apartmentShape.decoration == params_finish.id}">selected</c:if>>${params_finish.para_name}</option>
              			</c:forEach>
              		</select>
              	</td>
              	<td><input type="text" id="build_space_${s.index}" value="${apartmentShape.build_space}"></td>
              	<td><input type="text" id="inside_space_${s.index}" value="${apartmentShape.inside_space}"></td>
              	<td id="td_${s.index}">已保存</td>
              </tr>
            </c:forEach>
          	</tbody>
          </table>
        </form>
      </div>
    </div>
  </div>
<!-- content end -->

</div>

<a href="#" class="am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}">
  <span class="am-icon-btn am-icon-th-list"></span>
</a>

<jsp:include page="/foot.do" flush="true"/>

<!--[if lt IE 9]>
<script src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="assets/js/amazeui.ie8polyfill.min.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="assets/js/jquery.min.js"></script>
<!--<![endif]-->
<script src="assets/js/amazeui.min.js"></script>
<script src="assets/js/app.js"></script>
<script type="text/javascript">
$(function() {
	$("#add_button").click(function() {
		var i = $("input[name='apartmentShape_index']").length;
		var tr = "<tr>"+
					"<td><input name='apartmentShape_index' type='radio' value='"+i+"' />"+
					"<input type='hidden' id='id_"+i+"' value='0'></td>"+
					"<td><input type='text' id='code_"+i+"'></td>"+
					"<td><input type='text' id='unit_type_"+i+"'></td>"+
					"<td><select id='build_type_"+i+"'><c:forEach var="params_buildtype" items="${params_buildtypes}"><option value='${params_buildtype.id}'>${params_buildtype.para_name}</option></c:forEach></select></td>"+
					"<td><select id='params_finish_"+i+"'><c:forEach var="params_finish" items="${params_finishs}"><option value='${params_finish.id}'>${params_finish.para_name}</option></c:forEach></select></td>"+
					"<td><input type='text' id='build_space_"+i+"'></td>"+
					"<td><input type='text' id='inside_space_"+i+"'></td>"+
					"<td id='td_"+i+"'>未保存</td>"+
			  	 "</tr>";
		$("table").append(tr);
	});
	
	$("#save_button").click(function() {
		var i =  $('input[name="apartmentShape_index"]:checked').val();
		if(i==null || i == "undefined"){
			alert("请选择要操作的数据");
			return;
		}
		var saveData={"project_id":$("#project_id").val(),"id":$("#id_"+i).val(),"code":$("#code_"+i).val(),"unit_type":$("#unit_type_"+i).val(),"build_type":$("#build_type_"+i).val(),"decoration":$("#params_finish_"+i).val(),"build_space":$("#build_space_"+i).val(),"inside_space":$("#inside_space_"+i).val(),"status":"1"};
		$.ajax({
			contentType:"application/json",
		    type : "POST",
		    url : "<%=request.getContextPath()%>/save_apartmentShape.do",
		    data : JSON.stringify(saveData),
		    dataType: "json",   
		    success : function(data) {
		        if(data.flag=="1"){
		            alert("保存成功！");
		            $("#td_"+i).html("已保存");
		        }
		        else{
		            alert("保存失败！");
		        }
		    },
		    error :function(){
		        alert("网络连接出错！");
		    }
		});
	});
	
	$("#delete_button").click(function() {
		var i =  $('input[name="apartmentShape_index"]:checked').val();
		var saveData={"id":$("#id_"+i).val()};
		if($("#id_"+i).val()=="" || $("#id_"+i).val()=="undefined" || $("#id_"+i).val()==null){
			alert("此数据还没有保存，不用删除");
			return;
		}
		$.ajax({
			contentType:"application/json",
		    type : "POST",
		    url : "<%=request.getContextPath()%>/delete_apartmentShape.do",
		    data : JSON.stringify(saveData),
		    dataType: "json", 
		    success : function(data) {
		        if(data.flag=="1"){
		            alert("删除成功！");
		            $("#td_"+i).parent().remove();
		        }
		        else{
		            alert("删除失败！");
		        }
		    },
		    error :function(){
		        alert("网络连接出错！");
		    }
		});
	});
	
	$("#return_button").click(function() {
		window.location.href="/eosms/show_project.do";
	});
	
});

</script>
</body>
</html>