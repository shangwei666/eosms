<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!doctype html>
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>IFS管家</title>
  <meta name="description" content="IFS管家">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="IFS管家" />
  <link rel="stylesheet" href="assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="assets/css/admin.css">
</head>
<body>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<jsp:include page="/header.do" flush="true"/>

<div class="am-cf admin-main">
  <!-- sidebar start -->
  <jsp:include page="/tree.do" flush="true"/>
  <!-- sidebar end -->

<!-- content start -->
<div class="admin-content">

    <div class="am-cf am-padding">
      <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">楼宇</strong> / <small>building</small></div>
    </div>

    <div class="am-g">
      <div class="am-u-sm-12 am-u-md-6">
        <div class="am-btn-toolbar">
          <div class="am-btn-group am-btn-group-xs">
            <button type="button" id="add_button" class="am-btn am-btn-default"><span class="am-icon-plus"></span> 新增</button>
            <button type="button" id="save_button" class="am-btn am-btn-default"><span class="am-icon-save"></span> 保存</button>
            <button type="button" id="delete_button" class="am-btn am-btn-default"><span class="am-icon-trash-o"></span> 删除</button>
          </div>
        </div>
      </div>
      <div class="am-u-sm-12 am-u-md-3">
        <div class="am-form-group">
        &nbsp;
        </div>
      </div>
      <div class="am-u-sm-12 am-u-md-3">
		<div class="am-form-group">
        &nbsp;
        </div>
      </div>
    </div>

    <div class="am-g">
      <div class="am-u-sm-12">
        <form class="am-form">
        <input type="hidden" id="region_id" value="${region_id}">
          <table class="am-table am-table-bordered am-table-centered">
            <thead>
              <tr>
                <th class="table-check" width="3%"></th>
                <th class="table-title" width="50%">楼号</th>
                <th class="table-title" width="20%">状态</th>
                <th class="table-title" width="27%">操作</th>
              </tr>
          	</thead>
          	<tbody>
          	<c:forEach var="building" items="${buildings}" varStatus="s">
              <tr>
              	<td><input type="radio" name="building_index" value="${s.index}">
              	<input type="hidden" id="id_${s.index}" value="${building.id}">
              	</td>
              	<td><input type="text" id="building_num_${s.index}" value="${building.building_num}"></td>
              	<td id="td_${s.index}">已保存</td>
              	<td>
              	<button type="button" class="am-btn am-btn-primary am-btn-xs" onclick="add_unit(${s.index})">添加单元</button>
              	<button type="button" class="am-btn am-btn-primary am-btn-xs" onclick="create_apartment(${s.index})">生成房源</button>
              	<button type="button" class="am-btn am-btn-primary am-btn-xs" onclick="clear_apartment(${s.index})">清空房源</button>
              	</td>
              </tr>
            </c:forEach>
          	</tbody>
          </table>
        </form>
      </div>
    </div>
  </div>
<!-- content end -->

</div>

<a href="#" class="am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}">
  <span class="am-icon-btn am-icon-th-list"></span>
</a>

<jsp:include page="/foot.do" flush="true"/>

<!--[if lt IE 9]>
<script src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="assets/js/amazeui.ie8polyfill.min.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="assets/js/jquery.min.js"></script>
<!--<![endif]-->
<script src="assets/js/amazeui.min.js"></script>
<script src="assets/js/app.js"></script>
<script type="text/javascript">
$(function() {
	$("#add_button").click(function() {
		var i = $("input[name='building_index']").length;
		var tr = "<tr>"+
					"<td><input name='building_index' type='radio' value='"+i+"'>"+
					"<input type='hidden' id='id_"+i+"' value='0'></td>"+
					"<td><input type='text' id='building_num_"+i+"'></td>"+
					"<td id='td_"+i+"'>未保存</td>"+
					"<td><button type='button' class='am-btn am-btn-primary am-btn-xs' onclick='add_unit("+i+")'> 添加单元</button></td>"+
			  	 "</tr>";
		$("table").append(tr);
	});
	
	$("#save_button").click(function() {
		var i =  $('input[name="building_index"]:checked').val();
		var saveData={"building_num":$("#building_num_"+i).val(),"id":$("#id_"+i).val(),"region_id":$("#region_id").val(),"status":"1"};
		$.ajax({
			contentType:"application/json",
		    type : "POST",
		    url : "<%=request.getContextPath()%>/add_building.do",
		    data : JSON.stringify(saveData),
		    dataType: "json",   
		    success : function(data) {
		        if(data.flag=="1"){
		            alert("保存成功！");
		            $("#td_"+i).html("已保存");
		            $("#id_"+i).val(data.id);
		        }
		        else{
		            alert("保存失败！");
		        }
		    },
		    error :function(){
		        alert("网络连接出错！");
		    }
		});
	});
	
	
	$("#delete_button").click(function() {
		var i =  $('input[name="building_index"]:checked').val();
		var deleteData={"id":$("#id_"+i).val()};
		if($("#id_"+i).val()=="" || $("#id_"+i).val()=="undefined" || $("#id_"+i).val()==null){
			alert("此数据还没有保存，不用删除");
			return;
		}
		$.ajax({
			contentType:"application/json",
		    type : "POST",
		    url : "<%=request.getContextPath()%>/delete_building.do",
		    data : JSON.stringify(deleteData),
		    dataType: "json", 
		    success : function(data) {
		        if(data.flag=="1"){
		            alert("删除成功！");
		            $("#td_"+i).parent().remove();
		        }
		        else{
		            alert("删除失败！");
		        }
		    },
		    error :function(){
		        alert("网络连接出错！");
		    }
		});
	});
});

function add_unit(index){
	var building_id = $("#id_"+index).val();
	window.location.href="show_add_unit.do?building_id="+building_id;
}

function create_apartment(index){
	var building_id = $("#id_"+index).val();
	$.ajax({
	    type : "POST",
	    url : "<%=request.getContextPath()%>/create_apartment.do?building_id="+building_id,
	    success : function(data) {
	        if(data.flag=="1"){
	            alert("创建成功！");
	        }
	        else{
	            alert("创建失败！");
	        }
	    },
	    error :function(){
	        alert("网络连接出错！");
	    }
	});
}

function clear_apartment(index){
	var building_id = $("#id_"+index).val();
	$.ajax({
	    type : "POST",
	    url : "<%=request.getContextPath()%>/clear_apartment.do?building_id="+building_id,
	    success : function(data) {
	        if(data.flag=="1"){
	            alert("清空成功！");
	        }
	        else{
	            alert("清空失败！");
	        }
	    },
	    error :function(){
	        alert("网络连接出错！");
	    }
	});
}
</script>
</body>
</html>