<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!doctype html>
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Amaze UI Admin index Examples</title>
  <meta name="description" content="这是一个 index 页面">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="assets/css/admin.css">
</head>
<body>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<jsp:include page="/header.do" flush="true"/>

<div class="am-cf admin-main">
  <!-- sidebar start -->
  <jsp:include page="/tree.do" flush="true"/>
  <!-- sidebar end -->

<!-- content start -->
<div class="admin-content">

  <div class="am-cf am-padding">
    <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">客户</strong> / <small>client</small></div>
  </div>
<form id="client_form" class="am-form" action="show_client.do" method="post">
<input type="hidden" name="id" id="id" value="0">
<input type="hidden" name="page_num" id="page_num" value="0">
  <div class="am-tabs am-margin" data-am-tabs>
    <ul class="am-tabs-nav am-nav am-nav-tabs">
      <li class="am-active"><a href="#tab1">基本信息</a></li>
    </ul>


    <div class="am-tabs-bd">
      <div class="am-tab-panel am-fade am-in am-active" id="tab1">
          <div class="am-g am-margin-top">
            <div class="am-u-sm-1 am-u-md-1 am-text-right">
              	来访日期
            </div>
            <div class="am-u-sm-2 am-u-md-2">
            	<div class="am-input-group am-datepicker-date" data-am-datepicker="{format: 'yyyy-mm-dd'}">
				  <input type="text" class="am-form-field" name="visit_date" id="visit_date" readonly>
				  <span class="am-input-group-btn am-datepicker-add-on">
				    <button class="am-btn am-btn-default" type="button"><span class="am-icon-calendar"></span> </button>
				  </span>
				</div>
            </div>
            <div class="am-u-sm-1 am-u-md-1 am-text-right">
              	客户姓名
            </div>
            <div class="am-u-sm-2 am-u-md-2">
            	<input type="text" class="am-input-sm" name="name" id="name" value="${name}">
            </div>
            <div class="am-u-sm-1 am-u-md-1 am-text-right">
              	手机
            </div>
            <div class="am-u-sm-2 am-u-md-2">
            	<input type="text" class="am-input-sm" name="show_phone" id="show_phone" maxlength="11"  value="${show_phone}">
            	<input type="hidden" name="phone" id="phone">
            </div>
            <div class="am-u-sm-1 am-u-md-1 am-text-right">
              	来访类型
            </div>
            <div class="am-u-sm-2 am-u-md-2">
              	<select name="visit_type" id="visit_type" class="am-input-sm">
            		<option value="0">请选择</option>
              		<c:forEach var="visitType" items="${params_visitType}">
              			<option value="${visitType.id}">${visitType.para_name}</option>
              		</c:forEach>
              	</select>
            </div>
          </div>
          <div class="am-g am-margin-top">
            <div class="am-u-sm-1 am-u-md-1 am-text-right">
              	需求户型
            </div>
            <div class="am-u-sm-2 am-u-md-2">
            	<select name="hope_shape" id="hope_shape" class="am-input-sm">
            		<option value="0">请选择</option>
              		<c:forEach var="hopeShape" items="${params_hopeShape}">
              			<option value="${hopeShape.id}">${hopeShape.para_name}</option>
              		</c:forEach>
              	</select>
            </div>
            <div class="am-u-sm-1 am-u-md-1 am-text-right">
              	需求单价
            </div>
            <div class="am-u-sm-2 am-u-md-2">
            	<input type="number" class="am-input-sm" name="hope_price" id="hope_price" placeholder="单位元">
            </div>
            <div class="am-u-sm-1 am-u-md-1 am-text-right">
              	付款方式
            </div>
            <div class="am-u-sm-2 am-u-md-2">
            	<select name="pay_type" id="pay_type" class="am-input-sm">
            		<option value="0">请选择</option>
              		<c:forEach var="payType" items="${params_payType}">
              			<option value="${payType.id}">${payType.para_name}</option>
              		</c:forEach>
              	</select>
            </div>
            <div class="am-u-sm-1 am-u-md-1 am-text-right">
              	意向楼层
            </div>
            <div class="am-u-sm-2 am-u-md-2">
            	<input type="text" class="am-input-sm" name="hope_floor" id="hope_floor">
            </div>
          </div>
          <div class="am-g am-margin-top">
            <div class="am-u-sm-1 am-u-md-1 am-text-right">
              	购房用途
            </div>
            <div class="am-u-sm-2 am-u-md-2">
            	<select name="use_type" class="am-input-sm" id="use_type">
            		<option value="0">请选择</option>
              		<c:forEach var="useType" items="${params_useType}">
              			<option value="${useType.id}">${useType.para_name}</option>
              		</c:forEach>
              	</select>
            </div>
            <div class="am-u-sm-1 am-u-md-1 am-text-right">
              	认知途径
            </div>
            <div class="am-u-sm-2 am-u-md-2">
            	<select name="know_type" class="am-input-sm" id="know_type">
            		<option value="0">请选择</option>
              		<c:forEach var="knowType" items="${params_knowType}">
              			<option value="${knowType.id}">${knowType.para_name}</option>
              		</c:forEach>
              	</select>
            </div>
            <div class="am-u-sm-1 am-u-md-1 am-text-right">
              	职业
            </div>
            <div class="am-u-sm-2 am-u-md-2">
            	<select name="job_type" class="am-input-sm" id="job_type">
            		<option value="0">请选择</option>
              		<c:forEach var="jobType" items="${params_jobType}">
              			<option value="${jobType.id}">${jobType.para_name}</option>
              		</c:forEach>
              	</select>
            </div>
            <div class="am-u-sm-1 am-u-md-1 am-text-right">
              	家庭结构
            </div>
            <div class="am-u-sm-2 am-u-md-2">
            	<select name="family" class="am-input-sm" id="family">
            		<option value="0">请选择</option>
              		<c:forEach var="family" items="${params_family}">
              			<option value="${family.id}">${family.para_name}</option>
              		</c:forEach>
              	</select>
            </div>
          </div>
          <div class="am-g am-margin-top">
            <div class="am-u-sm-1 am-u-md-1 am-text-right">
              	客户区域
            </div>
            <div class="am-u-sm-2 am-u-md-2">
            	<select name="client_area" class="am-input-sm" id="client_area">
            		<option value="0">请选择</option>
              		<c:forEach var="clientArea" items="${params_clientArea}">
              			<option value="${clientArea.id}">${clientArea.para_name}</option>
              		</c:forEach>
              	</select>
            </div>
            <div class="am-u-sm-1 am-u-md-1 am-text-right">
              	客户关注
            </div>
            <div class="am-u-sm-2 am-u-md-2">
           		<select name="focus" class="am-input-sm" id="focus">
            		<option value="0">请选择</option>
              		<c:forEach var="foucsType" items="${params_foucsType}">
              			<option value="${foucsType.id}">${foucsType.para_name}</option>
              		</c:forEach>
              	</select>
            </div>
            <div class="am-u-sm-1 am-u-md-1 am-text-right">
              	经纪公司
            </div>
            <div class="am-u-sm-2 am-u-md-2">
            	<input type="text" class="am-input-sm" name="sales_company" id="sales_company">
            </div>
            <div class="am-u-sm-1 am-u-md-1 am-text-right">
              	置业顾问
            </div>
            <div class="am-u-sm-2 am-u-md-2">
            	<input type="text" class="am-input-sm" name="sales" id="sales">
            </div>
          </div>
      </div>
      <div class="am-margin am-text-right">
     	<button type="button" id="search_button" class="am-btn am-btn-primary am-btn-xs">查询</button>
	    <button type="button" id="save_button" class="am-btn am-btn-primary am-btn-xs">修改</button>
	    <button type="button" id="add_button" class="am-btn am-btn-primary am-btn-xs">新增</button>
	    <button type="button" id="clear_button" class="am-btn am-btn-primary am-btn-xs">清空</button>
	  </div>
    </div>
  </div>
</form>
  <div class="am-g">
      <div class="am-u-sm-12">
        <form class="am-form">
          <table id="clients_table" class="am-table am-table-striped am-table-hover table-main">
            <thead>
              <tr>
                <th class="table-title" width="14%">来访日期</th>
                <th class="table-title" width="14%">客户姓名</th>
                <th class="table-title" width="14%">手机号码</th>
                <th class="table-title" width="14%">来访类型</th>
                <th class="table-title" width="14%">需求户型</th>
                <th class="table-title" width="14%">需求单价</th>
                <th class="table-title" width="14%">付款方式</th>
              </tr>
          </thead>
          <tbody>
          
          <c:forEach var="client" items="${clients}" varStatus="s">
            <tr class="tr_click" client_id="${client.id}">
              <td>${client.visit_date}</td>
              <td>${client.name}</td>
              <td>${client.show_phone}</td>
              <td>
              	<c:forEach var="visitType" items="${params_visitType}">
          			<c:if test="${visitType.id == client.visit_type}">${visitType.para_name}</c:if>
          		</c:forEach>
              </td>
              <td>
              	<c:forEach var="hopeShape" items="${params_hopeShape}">
          			<c:if test="${hopeShape.id == client.hope_shape}">${hopeShape.para_name}</c:if>
          		</c:forEach>
              </td>
              <td>
              	${client.hope_price}
          	  </td>
              <td>
              	<c:forEach var="payType" items="${params_payType}">
          			<c:if test="${payType.id == client.pay_type}">${payType.para_name}</c:if>
          		</c:forEach>
              </td>
            </tr>
          </c:forEach>
          </tbody>
        </table>
          <div class="am-cf">
  共 ${page.allRow} 条记录
  <div class="am-fr">
    <ul class="am-pagination">
      <c:choose>
      <c:when test="${page.currentPage==1}">
      <li class="am-disabled"><a href="#">«</a></li>
      </c:when>
      <c:when test="${page.currentPage!=1}">
      <li><a href="javascript:void(0)" onclick="choose_page(${page.currentPage-1})">«</a></li>
      </c:when>
      </c:choose>
      <c:forEach var="i" begin="1" end="${page.totalPage}" step="1">
      <c:choose>
      <c:when test="${page.currentPage==i}">
      <li class="am-active"><a href="javascript:void(0)"><c:out value="${i}"/></a></li>
      </c:when>
      <c:when test="${page.currentPage!=i}">
      <li><a href="javascript:void(0)" onclick="choose_page(${i})"><c:out value="${i}"/></a></li>
      </c:when>
      </c:choose>
      </c:forEach>
      <c:choose>
      <c:when test="${page.currentPage==page.totalPage}">
      <li class="am-disabled"><a href="#">»</a></li>
      </c:when>
      <c:when test="${page.currentPage!=page.totalPage}">
      <li><a href="javascript:void(0)" onclick="choose_page(${page.currentPage+1})">»</a></li>
      </c:when>
      </c:choose>
    </ul>
  </div>
</div>
        </form>
      </div>
    </div>
</div>

<!-- content end -->

</div>

<a href="#" class="am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}">
  <span class="am-icon-btn am-icon-th-list"></span>
</a>

<jsp:include page="/foot.do" flush="true"/>

<!--[if lt IE 9]>
<script src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="assets/js/amazeui.ie8polyfill.min.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="assets/js/jquery.min.js"></script>
<!--<![endif]-->
<script src="assets/js/amazeui.min.js"></script>
<script src="assets/js/app.js"></script>
<script type="text/javascript">
$(function() {
	//页面初始化时默认新增客户，所以隐藏新增按钮
	$("#save_button").hide();//隐藏
	
	//保存按钮点击事件
	$("#save_button").click(function() {
		var saveData={"id":$("#id").val(),"visit_date":$("#visit_date").val(),"name":$("#name").val(),"show_phone":$("#show_phone").val(),"phone":$("#phone").val(),"sex":$("#sex").val(),"hope_shape":$("#hope_shape").val(),
				"hope_price":$("#hope_price").val(),"hope_floor":$("#hope_floor").val(),"focus":$("#focus").val(),"sales":$("#sales").val(),"sales_company":$("#sales_company").val(),
				"visit_type":$("#visit_type").val(),"pay_type":$("#pay_type").val(),"use_type":$("#use_type").val(),"know_type":$("#know_type").val(),"job_type":$("#job_type").val(),
				"family":$("#family").val(),"client_area":$("#client_area").val(),"status":1};
		$.ajax({
			contentType:"application/json",
		    type : "POST",
		    url : "<%=request.getContextPath()%>/add_client.do",
		    data : JSON.stringify(saveData),
		    dataType: "json",
		    success : function(data) {
		        if(data.flag=="1"){
		        	$("#id").val(data.id);
		        	var t01 = $("#clients_table tr").length;
		        	 $("#clients_table tr:gt(0):eq(t01-1)").remove();
		            alert("保存成功！");
		        }
		        else{
		            alert("保存失败！");
		        }
		    },
		    error :function(){
		        alert("网络连接出错！");
		    }
		});
	});
	
	//电话号码获得焦点事件
	$('#show_phone').focus(function() {
		 $('#show_phone').val( $('#phone').val());
	});
	
	//电话号码失去焦点事件
	$('#show_phone').blur(function() {
		 var show_phone = this.value;
		 if(show_phone.length != 11){
			 alert("请输入正确手机号码");
		 }
		 else{
			 var r,re;
		     re = /^[1][345678]\d{9}$/; //\d表示数字,*表示匹配多个数字
		     r = show_phone.match(re);
		     if(r!=show_phone){
		    	 alert("请输入正确手机号码");
		     }
		     else{
		  	 	 $("#show_phone").val(show_phone.substr(0,3)+"****"+show_phone.substr(7,10));
		    	 $('#phone').val(show_phone);
		     }
		 }
	 });
	 
	 //客户列表点击事件
	 $(".tr_click").click(function(){
		 $.ajax({
				contentType:"application/json",
			    type : "GET",
			    url : "<%=request.getContextPath()%>/get_client.do?id="+$(this).attr("client_id"),
			    dataType: "json",
			    success : function(data) {
			        if(data.flag=="1"){
			        	if(data.client.visit_type!=undefined && data.client.visit_type!=0){
			        		$("#visit_type").val(data.client.visit_type);
			        	}
			        	else{
			        		$("#visit_type").val(0);
			        	}
			        	if(data.client.hope_shape!=undefined && data.client.hope_shape!=0){
			        		$("#hope_shape").val(data.client.hope_shape);
			        	}
			        	else{
			        		$("#hope_shape").val(0);
			        	}
			        	if(data.client.pay_type!=undefined && data.client.pay_type!=0){
			        		$("#pay_type").val(data.client.pay_type);
			        	}
			        	else{
			        		$("#pay_type").val(0);
			        	}
			        	if(data.client.use_type!=undefined && data.client.use_type!=0){
			        		$("#use_type").val(data.client.use_type);
			        	}
			        	else{
			        		$("#use_type").val(0);
			        	}
			        	if(data.client.know_type!=undefined && data.client.know_type!=0){
			        		$("#know_type").val(data.client.know_type);
			        	}
			        	else{
			        		$("#know_type").val(0);
			        	}
			        	if(data.client.job_type!=undefined && data.client.job_type!=0){
			        		$("#job_type").val(data.client.job_type);
			        	}
			        	else{
			        		$("#job_type").val(0);
			        	}
			        	if(data.client.family!=undefined && data.client.family!=0){
			        		$("#family").val(data.client.family);
			        	}
			        	else{
			        		$("#family").val(0);
			        	}
			        	if(data.client.client_area!=undefined && data.client.client_area!=0){
			        		$("#client_area").val(data.client.client_area);
			        	}
			        	else{
			        		$("#client_area").val(0);
			        	}
			        	if(data.client.focus!=undefined && data.client.focus!=0){
			        		$("#focus").val(data.client.focus);
			        	}
			        	else{
			        		$("#focus").val(0);
			        	}
			        	$("#visit_date").val(data.client.visit_date);
			        	$("#name").val(data.client.name);
			        	$("#phone").val(data.client.phone);
			        	$("#show_phone").val(data.client.show_phone);
			        	$("#hope_price").val(data.client.hope_price);
			        	$("#hope_floor").val(data.client.hope_floor);
			        	$("#sales_company").val(data.client.sales_company);
			        	$("#sales").val(data.client.sales);
			        	$("#id").val(data.client.id);
			        }
			        else{
			            alert("保存失败！");
			        }
			    },
			    error :function(){
			        alert("网络连接出错！");
			    }
			});
		 $(".tr_click").removeClass("am-active");
		 $(this).addClass("am-active");
         $("#save_button").show();//显示
	 });
	 
	 //清空按钮点击事件
	 $("#clear_button").click(function(){
		 $(".tr_click").removeClass("am-active");
		 $("#id").val(0);
		 $('#client_form')[0].reset();
         $("#save_button").hide();//隐藏
	 });
	 
	//新增按钮点击事件
	 $("#add_button").click(function(){
		 $("#id").val(0);
		 $("#save_button").click();
		 $(".tr_click").removeClass("am-active");
	 });
	
	//查询按钮点击事件
	 $("#search_button").click(function(){
		 $("#client_form").submit();
	 });
});

function choose_page(i){
	$("#page_num").val(i);
	$("#client_form").submit();
}
</script>
</body>
</html>
