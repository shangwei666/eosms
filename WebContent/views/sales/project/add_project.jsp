<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!doctype html>
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Amaze UI Admin index Examples</title>
  <meta name="description" content="这是一个 index 页面">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="assets/css/admin.css">
</head>
<body>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<jsp:include page="/header.do" flush="true"/>

<div class="am-cf admin-main">
  <!-- sidebar start -->
  <jsp:include page="/tree.do" flush="true"/>
  <!-- sidebar end -->

<!-- content start -->
<div class="admin-content">

  <div class="am-cf am-padding">
    <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">表单</strong> / <small>form</small></div>
  </div>
<form id="project_form" class="am-form" action="save_project.do" method="post" enctype="multipart/form-data" >
<c:if test="${project!=null}">
<input type="hidden" name="id" id="id" value="${project.id}">
<input type="hidden" name="status" value="${project.status}">
</c:if>
<c:if test="${project==null}">
<input type="hidden" name="id" id="id" value="0">
<input type="hidden" name="status" value="1">
</c:if>
  <div class="am-tabs am-margin" data-am-tabs>
    <ul class="am-tabs-nav am-nav am-nav-tabs">
      <li class="am-active"><a href="#tab1">基本信息</a></li>
      <li><a href="#tab2">详细描述</a></li>
    </ul>


    <div class="am-tabs-bd">
      <div class="am-tab-panel am-fade am-in am-active" id="tab1">
          <div class="am-g am-margin-top">
            <div class="am-u-sm-4 am-u-md-2 am-text-right">
              项目楼盘全称
            </div>
            <div class="am-u-sm-8 am-u-md-4">
              <input type="text" class="am-input-sm" name="name" value="${project.name}">
            </div>
            <div class="am-hide-sm-only am-u-md-6">*必填</div>
          </div>

          <div class="am-g am-margin-top">
            <div class="am-u-sm-4 am-u-md-2 am-text-right">
              楼盘简称/宣传名称
            </div>
            <div class="am-u-sm-8 am-u-md-4 am-u-end col-end">
              <input type="text" class="am-input-sm" name="short_name" value="${project.short_name}">
            </div>
          </div>
          <div class="am-g am-margin-top">
            <div class="am-u-sm-4 am-u-md-2 am-text-right">
              项目俯视图
            </div>
            <div class="am-u-sm-8 am-u-md-4 am-u-end col-end">
            <c:choose>
			<c:when test="${project.image_url eq null || empty project.image_url}">
				<input type="file" name="file" id="user-pic">
             	<p class="am-form-help">请选择要上传的文件...</p>
			</c:when>
			<c:otherwise>
				<img class="am-img-thumbnail am-img-bdrs" src="fileUpload/${project.image_url}" alt=""/>
				<input type="hidden" name="image_url" value="${project.image_url}">
				<input type="file" name="file" id="user-pic">
             	<p class="am-form-help">${project.image_url}</p>
			</c:otherwise>
			</c:choose>            
            </div>
          </div>
      </div>

      <div class="am-tab-panel am-fade" id="tab2">
          <div class="am-g am-margin-top">
            <div class="am-u-sm-2 am-u-md-2 am-text-right">
              	物业类别
            </div>
            <div class="am-u-sm-4 am-u-md-4">
            	<select name="property_type" class="am-input-sm">
            		<option value="0">请选择</option>
              		<c:forEach var="propertyType" items="${params_propertyType}">
              			<option value="${propertyType.id}" <c:if test="${propertyType.id == project.property_type}">selected</c:if>>${propertyType.para_name}</option>
              		</c:forEach>
              	</select>
            </div>
            <div class="am-u-sm-2 am-u-md-2 am-text-right">
              	售楼处地址
            </div>
            <div class="am-u-sm-4 am-u-md-4">
              <input type="text" class="am-input-sm" name="sales_offices_address" value="${project.sales_offices_address}">
            </div>
          </div>
          
          <div class="am-g am-margin-top">
            <div class="am-u-sm-2 am-u-md-2 am-text-right">
             	 建筑类别
            </div>
            <div class="am-u-sm-4 am-u-md-4">
              <select name="build_type" class="am-input-sm">
              		<option value="0">请选择</option>
              	<c:forEach var="buildType" items="${params_buildType}">
              		<option value="${buildType.id}" <c:if test="${buildType.id == project.build_type}">selected</c:if>>${buildType.para_name}</option>
              	</c:forEach>
              </select>
            </div>
            <div class="am-u-sm-2 am-u-md-2 am-text-right">
             	 物业地址
            </div>
            <div class="am-u-sm-4 am-u-md-4">
              <input type="text" class="am-input-sm" name="property_offices_address" value="${project.property_offices_address}">
            </div>
          </div>
          
          <div class="am-g am-margin-top">
            <div class="am-u-sm-2 am-u-md-2 am-text-right admin-form-text">
              	环境位置
            </div>
            <div class="am-u-sm-4 am-u-md-4">
              <input type="text" class="am-input-sm" name="area" value="${project.area}">
            </div>
            <div class="am-u-sm-2 am-u-md-2 am-text-right">
              	所属商圈
            </div>
            <div class="am-u-sm-4 am-u-md-4">
              	<select name="business_district" class="am-input-sm">
            		<option value="0">请选择</option>
              		<c:forEach var="businessDistrict" items="${params_businessDistrict}">
              			<option value="${businessDistrict.id}" <c:if test="${businessDistrict.id == project.business_district}">selected</c:if>>${businessDistrict.para_name}</option>
              		</c:forEach>
              	</select>
            </div>
          </div>
          
          <div class="am-g am-margin-top">
            <div class="am-u-sm-4 am-u-md-2 am-text-right">
              	装修状况
            </div>
            <div class="am-u-sm-8 am-u-md-4">
              <select name="fitment" class="am-input-sm">
            		<option value="0">请选择</option>
              		<c:forEach var="fitmentType" items="${params_fitmentType}">
              			<option value="${fitmentType.id}" <c:if test="${fitmentType.id == project.fitment}">selected</c:if>>${fitmentType.para_name}</option>
              		</c:forEach>
              	</select>
            </div>
            <div class="am-u-sm-4 am-u-md-2 am-text-right">
            	  容积率
            </div>
            <div class="am-u-sm-8 am-u-md-4">
              <input type="text" class="am-input-sm" name="plot_ratio" value="${project.plot_ratio}">
            </div>
          </div>
          
          <div class="am-g am-margin-top">
            <div class="am-u-sm-2 am-u-md-2 am-text-right">
              	开盘时间
            </div>
            <div class="am-u-sm-4 am-u-md-4">
              	<div class="am-input-group am-datepicker-date" data-am-datepicker="{format: 'yyyy-mm-dd'}">
				  <input type="text" class="am-form-field"  name="open_date" value="<fmt:formatDate value="${project.open_date}" type="date" dateStyle="default"/>" readonly>
				  <span class="am-input-group-btn am-datepicker-add-on">
				    <button class="am-btn am-btn-default" type="button"><span class="am-icon-calendar"></span> </button>
				  </span>
				</div>
            </div>
            <div class="am-u-sm-2 am-u-md-2 am-text-right">
              	交房时间
            </div>
            <div class="am-u-sm-4 am-u-md-4">
            	<div class="am-input-group am-datepicker-date" data-am-datepicker="{format: 'yyyy-mm-dd'}">
				  <input type="text" class="am-form-field"  name="close_date" value="<fmt:formatDate value="${project.close_date}" type="date" dateStyle="default"/>" readonly>
				  <span class="am-input-group-btn am-datepicker-add-on">
				    <button class="am-btn am-btn-default" type="button"><span class="am-icon-calendar"></span> </button>
				  </span>
				</div>
            </div>
          </div>
          
          <div class="am-g am-margin-top">
            <div class="am-u-sm-2 am-u-md-2 am-text-right">
             	 绿化率
            </div>
            <div class="am-u-sm-4 am-u-md-4">
              <input type="text" class="am-input-sm" name="greening_rate" value="${project.greening_rate}">
            </div>
            <div class="am-u-sm-2 am-u-md-2 am-text-right">
              	物业费
            </div>
            <div class="am-u-sm-4 am-u-md-4">
              <input type="text" class="am-input-sm" name="property_fee" id="property_fee" value="${project.property_fee}">
            </div>
          </div>
          
          <div class="am-g am-margin-top">
            <div class="am-u-sm-2 am-u-md-2 am-text-right">
             	 物业公司
            </div>
            <div class="am-u-sm-4 am-u-md-4">
              <input type="text" class="am-input-sm" name="property_offices" value="${project.property_offices}">
            </div>
            <div class="am-u-sm-2 am-u-md-2 am-text-right">
              	开发商
            </div>
            <div class="am-u-sm-4 am-u-md-4">
              <input type="text" class="am-input-sm" name="land_developer" value="${project.land_developer}">
            </div>
          </div>
          
          <div class="am-g am-margin-top">
            <div class="am-u-sm-2 am-u-md-2 am-text-right">
              	预算许可证
            </div>
            <div class="am-u-sm-4 am-u-md-4">
              <input type="text" class="am-input-sm" name="presell_licence" value="${project.presell_licence}">
            </div>
            <div class="am-u-sm-2 am-u-md-2 am-text-right">
             	 项目均价
            </div>
            <div class="am-u-sm-4 am-u-md-4">
              <input type="text" class="am-input-sm" name="average_price" id="average_price" value="${project.average_price}">
            </div>
          </div>
      </div>
    </div>
  </div>
</form>
  <div class="am-margin">
    <button type="button" class="am-btn am-btn-primary am-btn-xs" onclick="do_submit()">提交保存</button>
    <button type="button" class="am-btn am-btn-primary am-btn-xs" onclick="do_back()">放弃保存</button>
  </div>
</div>
<!-- content end -->

</div>

<a href="#" class="am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}">
  <span class="am-icon-btn am-icon-th-list"></span>
</a>

<jsp:include page="/foot.do" flush="true"/>

<!--[if lt IE 9]>
<script src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="assets/js/amazeui.ie8polyfill.min.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="assets/js/jquery.min.js"></script>
<!--<![endif]-->
<script src="assets/js/amazeui.min.js"></script>
<script src="assets/js/app.js"></script>
<script type="text/javascript">
function do_submit(){
	$("#average_price").val(0);
	$("#property_fee").val(0);
	$("#project_form").submit();
	
}
</script>
</body>
</html>
