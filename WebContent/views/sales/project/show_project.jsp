<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!doctype html>
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>IFS管家</title>
  <meta name="description" content="IFS管家">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="IFS管家" />
  <link rel="stylesheet" href="assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="assets/css/admin.css">
</head>
<body>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<jsp:include page="/header.do" flush="true"/>
<div class="am-cf admin-main">
  <!-- sidebar start -->
  <jsp:include page="/tree.do" flush="true"/>
  <!-- sidebar end -->

<!-- content start -->
<div class="admin-content">

    <div class="am-cf am-padding">
      <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">项目列表</strong> / <small>project</small></div>
    </div>

    <div class="am-g">
      <div class="am-u-sm-12 am-u-md-6">
        <div class="am-btn-toolbar">
          <div class="am-btn-group am-btn-group-xs">
            <button type="button" id="add_button" class="am-btn am-btn-default"><span class="am-icon-plus"></span> 新增</button>
            <button type="button" id="update_button" class="am-btn am-btn-default"><span class="am-icon-save"></span> 修改</button>
            <button type="button" id="open_button" class="am-btn am-btn-default"><span class="am-icon-archive"></span> 开售</button>
            <button type="button" id="close_button" class="am-btn am-btn-default"><span class="am-icon-trash-o"></span> 停售</button>
          </div>
        </div>
      </div>
      <div class="am-u-sm-12 am-u-md-3">
        <div class="am-form-group">
          <select data-am-selected="{btnSize: 'sm'}">
            <option value="option1">所有类别</option>
            <option value="option2">别墅</option>
            <option value="option3">洋房</option>
            <option value="option3">板楼</option>
            <option value="option3">高层</option>
            <option value="option3">公寓</option>
            <option value="option3">独栋</option>
          </select>
        </div>
      </div>
      <div class="am-u-sm-12 am-u-md-3">
        <div class="am-input-group am-input-group-sm">
          <input type="text" class="am-form-field">
          <span class="am-input-group-btn">
            <button class="am-btn am-btn-default" type="button">搜索</button>
          </span>
        </div>
      </div>
    </div>

    <div class="am-g">
      <div class="am-u-sm-12">
        <form class="am-form">
          <table class="am-table am-table-striped am-table-hover table-main">
            <thead>
              <tr>
                <th class="table-check" width="5%"></th>
                <th class="table-title" width="35%">项目全称</th>
                <th class="table-type" width="15%">项目简称</th>
                <th class="table-author am-hide-sm-only" width="15%">开盘日期</th>
                <th class="table-date am-hide-sm-only" width="15%">交房日期</th>
                <th class="table-set" width="20%">操作</th>
              </tr>
          </thead>
          <tbody>
          <c:forEach var="project" items="${projects}" varStatus="s">
            <tr>
              <td><input type="radio" name="project_index" value="${s.index}">
              <input type="hidden" id="id_${s.index}" value="${project.id}"></td>
              <td><a href="show_add_project.do?id=${project.id}"><c:out value="${project.name}"/></a></td>
              <td><c:out value="${project.short_name}"/></td>
              <td class="am-hide-sm-only"><fmt:formatDate value="${project.open_date}" type="date" dateStyle="default"/></td>
              <td class="am-hide-sm-only"><fmt:formatDate value="${project.close_date}" type="date" dateStyle="default"/></td>
              <td>
                <div class="am-btn-toolbar">
                  <div class="am-btn-group am-btn-group-xs">
                    <button type="button" class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="show_add_apartmentShape(${project.id})"><span class="am-icon-pencil-square-o"></span> 户型</button>
                    <button type="button" class="am-btn am-btn-default am-btn-xs am-hide-sm-only" onclick="show_add_region(${project.id})"><span class="am-icon-copy"></span> 分区</button>
                    <button type="button" class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" onclick="sale_manage_region(${project.id})"><span class="am-icon-align-justify"></span> 销控</button>
                  </div>
                </div>
              </td>
            </tr>
            </c:forEach>
          </tbody>
        </table>
          <div class="am-cf">
  共 ${page.allRow} 条记录
  <div class="am-fr">
    <ul class="am-pagination">
      <c:choose>
      <c:when test="${page.currentPage==1}">
      <li class="am-disabled"><a href="#">«</a></li>
      </c:when>
      <c:when test="${page.currentPage!=1}">
      <li><a href="${page.currentPage-1}">«</a></li>
      </c:when>
      </c:choose>
      <c:forEach var="i" begin="1" end="${page.totalPage}" step="1">
      <c:choose>
      <c:when test="${page.currentPage==i}">
      <li class="am-active"><a href="${i}"><c:out value="${i}" /></a></li>
      </c:when>
      <c:when test="${page.currentPage!=i}">
      <li><a href="${i}"><c:out value="${i}" /></a></li>
      </c:when>
      </c:choose>
      </c:forEach>
      <c:choose>
      <c:when test="${page.currentPage==page.totalPage}">
      <li class="am-disabled"><a href="#">»</a></li>
      </c:when>
      <c:when test="${page.currentPage!=page.totalPage}">
      <li><a href="${page.currentPage+1}">»</a></li>
      </c:when>
      </c:choose>
    </ul>
  </div>
</div>
          <hr />
          <p>注：所有项目</p>
        </form>
      </div>

    </div>
  </div>
<!-- content end -->

</div>

<a href="#" class="am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}">
  <span class="am-icon-btn am-icon-th-list"></span>
</a>

<jsp:include page="/foot.do" flush="true"/>

<!--[if lt IE 9]>
<script src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="assets/js/amazeui.ie8polyfill.min.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="assets/js/jquery.min.js"></script>
<!--<![endif]-->
<script src="assets/js/amazeui.min.js"></script>
<script src="assets/js/app.js"></script>
<script type="text/javascript">
$(function() {
	$("#add_button").click(function() {
		window.location.href="/eosms/show_add_project.do";
	});
	
	$("#update_button").click(function() {
		var i =  $('input[name="project_index"]:checked').val();
		window.location.href="/eosms/show_add_project.do?id="+$("#id_"+i).val();
	});
});
function show_add_apartmentShape(id){
	window.location.href="/eosms/show_add_apartmentShape.do?project_id="+id; 
}
function show_add_region(id){
	window.location.href="/eosms/show_add_region.do?project_id="+id; 
}
function sale_manage_region(id){
	window.location.href="/eosms/sale_manage_region.do?project_id="+id; 
}
</script>
</body>
</html>
