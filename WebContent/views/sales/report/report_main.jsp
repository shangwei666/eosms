<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!doctype html>
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>IFS管家</title>
  <meta name="description" content="IFS管家">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="IFS管家" />
  <link rel="stylesheet" href="assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="assets/css/admin.css">
</head>
<body>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<jsp:include page="/header.do" flush="true"/>

<div class="am-cf admin-main">
  <!-- sidebar start -->
  <jsp:include page="/tree.do" flush="true"/>
  <!-- sidebar end -->

<!-- content start -->
<div class="admin-content">

    <div class="am-cf am-padding">
      <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">报表</strong> / <small>图形显示</small></div>
    </div>

    <div class="am-g">
      <div class="am-u-md-6">
        <div class="am-panel am-panel-default">
          <div class="am-panel-hd am-cf" data-am-collapse="{target: '#collapse-panel-1'}">柱状图<span class="am-icon-chevron-down am-fr" ></span></div>
          <div class="am-panel-bd am-collapse am-in" id="collapse-panel-1">
            <ul class="am-list admin-content-file">
              <li>
				<div id="zhutu" style="width: 600px;height:400px;"></div>
              </li>
            </ul>
          </div>
        </div>
        <div class="am-panel am-panel-default">
          <div class="am-panel-hd am-cf" data-am-collapse="{target: '#collapse-panel-2'}">折线图<span class="am-icon-chevron-down am-fr" ></span></div>
          <div class="am-panel-bd am-collapse am-in" id="collapse-panel-2">
            <ul class="am-list admin-content-file">
              <li>
				<div id="xiantu" style="width: 600px;height:400px;"></div>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div class="am-u-md-6">
        <div class="am-panel am-panel-default">
          <div class="am-panel-hd am-cf" data-am-collapse="{target: '#collapse-panel-4'}">饼状图<span class="am-icon-chevron-down am-fr" ></span></div>
          <div id="collapse-panel-4" class="am-panel-bd am-collapse am-in">
            <ul class="am-list admin-content-file">
              <li>
               <div id="bingtu" style="width: 600px;height:400px;"></div>
              </li>
            </ul>
          </div>
        </div>

        <div class="am-panel am-panel-default">
          <div class="am-panel-hd am-cf" data-am-collapse="{target: '#collapse-panel-3'}">漏斗图<span class="am-icon-chevron-down am-fr" ></span></div>
          <div id="collapse-panel-3" class="am-panel-bd am-collapse am-in">
            <ul class="am-list admin-content-file">
              <li>
               <div id="ditu" style="width: 600px;height:400px;"></div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- content end -->

</div>

<a href="#" class="am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}">
  <span class="am-icon-btn am-icon-th-list"></span>
</a>

<jsp:include page="/foot.do" flush="true"/>

<!--[if lt IE 9]>
<script src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="assets/js/amazeui.ie8polyfill.min.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="assets/js/jquery.min.js"></script>
<!--<![endif]-->
<script src="assets/js/amazeui.min.js"></script>
<script src="assets/js/app.js"></script>
<script src="assets/js/echarts/echarts.min.js"></script>
<script src="assets/js/echarts/china.js"></script>
<script type="text/javascript">
// 基于准备好的dom，初始化echarts实例
var zhutu = echarts.init(document.getElementById('zhutu'));
// 指定图表的配置项和数据
var zhutu_option = {
    title: {
        text: '月均销量'
    },
    tooltip: {},
    legend: {
        data:['销量']
    },
    xAxis: {
        data: ["一月","二月","三月","四月","五月","六月"]
    },
    yAxis: {},
    series: [{
        name: '销量',
        type: 'bar',
        data: [5, 20, 36, 10, 10, 20]
    }]
};

// 使用刚指定的配置项和数据显示图表。
zhutu.setOption(zhutu_option);

var bingtu = echarts.init(document.getElementById('bingtu'));
var bingtu_option = {
	    backgroundColor: '#2c343c',

	    title: {
	        text: 'Customized Pie',
	        left: 'center',
	        top: 20,
	        textStyle: {
	            color: '#ccc'
	        }
	    },

	    tooltip : {
	        trigger: 'item',
	        formatter: "{a} <br/>{b} : {c} ({d}%)"
	    },

	    visualMap: {
	        show: false,
	        min: 80,
	        max: 600,
	        inRange: {
	            colorLightness: [0, 1]
	        }
	    },
	    series : [
	        {
	            name:'访问来源',
	            type:'pie',
	            radius : '55%',
	            center: ['50%', '50%'],
	            data:[
	                {value:335, name:'直接访问'},
	                {value:310, name:'邮件营销'},
	                {value:274, name:'联盟广告'},
	                {value:235, name:'视频广告'},
	                {value:400, name:'搜索引擎'}
	            ].sort(function (a, b) { return a.value - b.value}),
	            roseType: 'angle',
	            label: {
	                normal: {
	                    textStyle: {
	                        color: 'rgba(255, 255, 255, 0.3)'
	                    }
	                }
	            },
	            labelLine: {
	                normal: {
	                    lineStyle: {
	                        color: 'rgba(255, 255, 255, 0.3)'
	                    },
	                    smooth: 0.2,
	                    length: 10,
	                    length2: 20
	                }
	            },
	            itemStyle: {
	                normal: {
	                    color: '#c23531',
	                    shadowBlur: 200,
	                    shadowColor: 'rgba(0, 0, 0, 0.5)'
	                }
	            }
	        }
	    ]
	};
bingtu.setOption(bingtu_option);

var ditu = echarts.init(document.getElementById('ditu'));
var ditu_option = {
	    title: {
	        text: '漏斗图',
	        subtext: '纯属虚构',
	        left: 'left',
	        top: 'bottom'
	    },
	    tooltip: {
	        trigger: 'item',
	        formatter: "{a} <br/>{b} : {c}%"
	    },
	    toolbox: {
	        orient: 'vertical',
	        top: 'center',
	        feature: {
	            dataView: {readOnly: false},
	            restore: {},
	            saveAsImage: {}
	        }
	    },
	    legend: {
	        orient: 'vertical',
	        left: 'left',
	        data: ['展现','点击','访问','咨询','订单']
	    },
	    calculable: true,
	    series: [
	        {
	            name: '漏斗图',
	            type: 'funnel',
	            width: '40%',
	            height: '45%',
	            left: '5%',
	            top: '50%',
	            data:[
	                {value: 60, name:'访问'},
	                {value: 30, name:'咨询'},
	                {value: 10, name:'订单'},
	                {value: 80, name:'点击'},
	                {value: 100, name:'展现'}
	            ]
	        },
	        {
	            name: '金字塔',
	            type: 'funnel',
	            width: '40%',
	            height: '45%',
	            left: '5%',
	            top: '5%',
	            sort: 'ascending',
	            data:[
	                {value: 60, name:'访问'},
	                {value: 30, name:'咨询'},
	                {value: 10, name:'订单'},
	                {value: 80, name:'点击'},
	                {value: 100, name:'展现'}
	            ]
	        },
	        {
	            name: '漏斗图',
	            type:'funnel',
	            width: '40%',
	            height: '45%',
	            left: '55%',
	            top: '5%',
	            label: {
	                normal: {
	                    position: 'left'
	                }
	            },
	            data:[
	                {value: 60, name: '访问'},
	                {value: 30, name: '咨询'},
	                {value: 10, name: '订单'},
	                {value: 80, name: '点击'},
	                {value: 100, name: '展现'}
	            ]
	        },
	        {
	            name: '金字塔',
	            type:'funnel',
	            width: '40%',
	            height: '45%',
	            left: '55%',
	            top: '50%',
	            sort: 'ascending',
	            label: {
	                normal: {
	                    position: 'left'
	                }
	            },
	            data:[
	                {value: 60, name: '访问'},
	                {value: 30, name: '咨询'},
	                {value: 10, name: '订单'},
	                {value: 80, name: '点击'},
	                {value: 100, name: '展现'}
	            ]
	        }
	    ]
	};
ditu.setOption(ditu_option);

var xiantu = echarts.init(document.getElementById('xiantu'));
var xiantu_option = {
	    title: {
	        text: '渠道'
	    },
	    tooltip : {
	        trigger: 'axis'
	    },
	    legend: {
	        data:['邮件营销','联盟广告','视频广告','直接访问','搜索引擎']
	    },
	    toolbox: {
	        feature: {
	            saveAsImage: {}
	        }
	    },
	    grid: {
	        left: '3%',
	        right: '4%',
	        bottom: '3%',
	        containLabel: true
	    },
	    xAxis : [
	        {
	            type : 'category',
	            boundaryGap : false,
	            data : ['周一','周二','周三','周四','周五','周六','周日']
	        }
	    ],
	    yAxis : [
	        {
	            type : 'value'
	        }
	    ],
	    series : [
	        {
	            name:'邮件营销',
	            type:'line',
	            stack: '总量',
	            areaStyle: {normal: {}},
	            data:[120, 132, 101, 134, 90, 230, 210]
	        },
	        {
	            name:'联盟广告',
	            type:'line',
	            stack: '总量',
	            areaStyle: {normal: {}},
	            data:[220, 182, 191, 234, 290, 330, 310]
	        },
	        {
	            name:'视频广告',
	            type:'line',
	            stack: '总量',
	            areaStyle: {normal: {}},
	            data:[150, 232, 201, 154, 190, 330, 410]
	        },
	        {
	            name:'直接访问',
	            type:'line',
	            stack: '总量',
	            areaStyle: {normal: {}},
	            data:[320, 332, 301, 334, 390, 330, 320]
	        },
	        {
	            name:'搜索引擎',
	            type:'line',
	            stack: '总量',
	            label: {
	                normal: {
	                    show: true,
	                    position: 'top'
	                }
	            },
	            areaStyle: {normal: {}},
	            data:[820, 932, 901, 934, 1290, 1330, 1320]
	        }
	    ]
	};
xiantu.setOption(xiantu_option);
</script>
</body>
</html>