package com.huanying.framework.company;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.huanying.framework.BaseDao;
import com.huanying.framework.PageBean;

@Service
public class CompanyServiceImpl implements CompanyService {
	
	Logger logger = Logger.getLogger(CompanyServiceImpl.class);
	
	@Autowired
	private BaseDao dao;

	@Override
	public void add(Company company) throws Exception {
		dao.save(company);
	}

	@Override
	public void update(Company company) throws Exception {
		dao.update(company);
	}

	@Override
	public void delete(Company company) throws Exception {
		dao.delete(company);
	}

	@Override
	public Company getbyId(int id) throws Exception {
		return (Company)dao.load(Company.class, id);
	}

	@Override
	public PageBean searchCompanies(String name, int pageSize, int page) throws Exception {
		StringBuilder hql = new StringBuilder("from Company where status=1");
		List<Object> params = new ArrayList<Object>();
		if(name!=null && !("").equals(name)){
			hql.append(" and name like ?");
			params.add("%"+name+"%");
		}
		
		int allRow = dao.queryAllRowCount(hql.toString(), params);
		int totalPage = PageBean.countTotalPage(pageSize, allRow);
		final int offset = PageBean.countOffset(pageSize, page);
	    final int currentPage = PageBean.countCurrentPage(page);
	    List<Object> list = dao.queryForPageAndParams(hql.toString(), params, offset, pageSize);
	    
	    //把分页信息保存到Bean中
        PageBean pageBean = new PageBean();
        pageBean.setPageSize(pageSize);    
        pageBean.setCurrentPage(currentPage);
        pageBean.setAllRow(allRow);
        pageBean.setTotalPage(totalPage);
        pageBean.setList(list);
        pageBean.init();
		
		return pageBean;
	}

}
