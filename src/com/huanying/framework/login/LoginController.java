package com.huanying.framework.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.huanying.framework.BaseController;
import com.huanying.framework.user.User;
import com.huanying.framework.user.UserService;

@Controller
public class LoginController extends BaseController {
	@Autowired
	private UserService userService;
	
	@RequestMapping("/login.do")
	public String login() {
		return "login";
	}
	
	@RequestMapping("/do_login.do")
	public String do_login(User user,Model model) throws Exception {
		String returnUrl;
		User user_obj = userService.findUser(user);
		if(user_obj == null){
			returnUrl = "redirect:login.do";
		}
		else{
			getSession().setAttribute("user", user);
			returnUrl = "redirect:index.do";
		}
		return returnUrl;
	}
	
	
	@RequestMapping("/redirect")
	public String redirect(){
	    return "redirect:hello";
	}
}
