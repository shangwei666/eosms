package com.huanying.framework.parameter;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Parameter implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1433196216342546584L;
	
	public static final String BUILD_TYPE = "001";			//建筑类型
	public static final String FITMENT_TYPE = "002";		//装修标准
	public static final String SALES_STATUS = "003";		//销售状态
	public static final String PROPERTY_TYPE = "004";		//物业类别
	public static final String BUSINESS_DISTRICT = "005";	//所属商圈
	public static final String VISIT_TYPE = "006";			//访客类型
	public static final String PAY_TYPE = "007";			//支付方式
	public static final String USE_TYPE = "008";			//购房用途
	public static final String KNOW_TYPE = "009";			//认知途径
	public static final String JOB_TYPE = "010";			//职业
	public static final String FAMILY = "011";				//家庭构成
	public static final String CLIENT_AREA = "012";			//客户区域
	public static final String FOUCS_TYPE = "013";			//客户关注点
	public static final String HOPE_SHAPE = "014";			//需求户型
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String para_name;
	private String para_value;
	private String para_type_code;
	@Column(name="status",nullable=false,columnDefinition="INT default 1")
	private int status;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPara_name() {
		return para_name;
	}
	public void setPara_name(String para_name) {
		this.para_name = para_name;
	}
	public String getPara_value() {
		return para_value;
	}
	public void setPara_value(String para_value) {
		this.para_value = para_value;
	}
	public String getPara_type_code() {
		return para_type_code;
	}
	public void setPara_type_code(String para_type_code) {
		this.para_type_code = para_type_code;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	
}
