package com.huanying.framework.parameter;

import java.util.List;

public interface ParameterService {
	
	public List findParasByType(String para_type_code) throws Exception;
	
	public Parameter findPara() throws Exception;
	
}
