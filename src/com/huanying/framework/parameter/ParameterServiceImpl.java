package com.huanying.framework.parameter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.huanying.framework.BaseDao;

@Service
public class ParameterServiceImpl implements ParameterService {
	@Autowired
	private BaseDao dao;
	
	@Override
	public List findParasByType(String para_type_code) throws Exception {
		String hql = "from Parameter where para_type_code=:para_type_code and status=1";
		Parameter para = new Parameter();
		para.setPara_type_code(para_type_code);
		List para_list = dao.exculeHql(hql, para);
		return para_list;
	}

	@Override
	public Parameter findPara() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
