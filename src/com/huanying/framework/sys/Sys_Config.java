package com.huanying.framework.sys;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Sys_Config implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6357318269794827620L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
	
	private String param_name;
	private String param_value;
	private String param_remark;
	
	@Column(name="status",nullable=false,columnDefinition="INT default 1")
	private int status;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getParam_name() {
		return param_name;
	}
	public void setParam_name(String param_name) {
		this.param_name = param_name;
	}
	public String getParam_value() {
		return param_value;
	}
	public void setParam_value(String param_value) {
		this.param_value = param_value;
	}
	public String getParam_remark() {
		return param_remark;
	}
	public void setParam_remark(String param_remark) {
		this.param_remark = param_remark;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

}
