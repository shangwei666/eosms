package com.huanying.sales.apartment;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Apartment implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4454972626688231817L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int project_id;
	private int region_id;
	private int building_id;
	private int unit_id;
	private int apartmentShape_id;
	private int client_id;
	
	private String name;	//房屋名称
	private int floor;		//楼层
	private BigDecimal unit_price;	//单价
	private BigDecimal total_price;	//总价
	private int sale_status;	//销售状态	1开发商留房  2可售  3小订  4大订  5首付交清  6银行放款  6销售完成
	private String build_space;		//建筑面积
	private String inside_space;	//套内面积
	private int isHaveModel;		//是否有样板间	0没有   1有
	@Column(name="status",nullable=false,columnDefinition="INT default 1")
	private int status;			//状态    0删除   1正常
	
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getProject_id() {
		return project_id;
	}


	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}


	public int getRegion_id() {
		return region_id;
	}


	public void setRegion_id(int region_id) {
		this.region_id = region_id;
	}


	public int getBuilding_id() {
		return building_id;
	}


	public void setBuilding_id(int building_id) {
		this.building_id = building_id;
	}


	public int getUnit_id() {
		return unit_id;
	}


	public void setUnit_id(int unit_id) {
		this.unit_id = unit_id;
	}


	public int getApartmentShape_id() {
		return apartmentShape_id;
	}


	public void setApartmentShape_id(int apartmentShape_id) {
		this.apartmentShape_id = apartmentShape_id;
	}


	public int getClient_id() {
		return client_id;
	}


	public void setClient_id(int client_id) {
		this.client_id = client_id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getFloor() {
		return floor;
	}


	public void setFloor(int floor) {
		this.floor = floor;
	}


	public BigDecimal getUnit_price() {
		return unit_price;
	}


	public void setUnit_price(BigDecimal unit_price) {
		this.unit_price = unit_price;
	}


	public BigDecimal getTotal_price() {
		return total_price;
	}


	public void setTotal_price(BigDecimal total_price) {
		this.total_price = total_price;
	}


	public int getSale_status() {
		return sale_status;
	}


	public void setSale_status(int sale_status) {
		this.sale_status = sale_status;
	}


	public String getBuild_space() {
		return build_space;
	}


	public void setBuild_space(String build_space) {
		this.build_space = build_space;
	}


	public String getInside_space() {
		return inside_space;
	}


	public void setInside_space(String inside_space) {
		this.inside_space = inside_space;
	}


	public int getIsHaveModel() {
		return isHaveModel;
	}


	public void setIsHaveModel(int isHaveModel) {
		this.isHaveModel = isHaveModel;
	}


	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}

}
