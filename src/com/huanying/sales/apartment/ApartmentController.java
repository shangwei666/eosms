package com.huanying.sales.apartment;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.huanying.framework.BaseController;
import com.huanying.framework.parameter.ParameterService;
import com.huanying.sales.apartmentshape.ApartmentShape;
import com.huanying.sales.apartmentshape.ApartmentShapeService;
import com.huanying.sales.building.Building;
import com.huanying.sales.building.BuildingController;
import com.huanying.sales.building.BuildingService;
import com.huanying.sales.client.Client;
import com.huanying.sales.client.ClientService;
import com.huanying.sales.project.Project;
import com.huanying.sales.project.ProjectService;
import com.huanying.sales.region.Region;
import com.huanying.sales.region.RegionService;
import com.huanying.sales.unit.Unit;
import com.huanying.sales.unit.UnitService;
import com.huanying.sales.unit.Unit_Apartmentshape;

@Controller
public class ApartmentController extends BaseController {
	@Autowired  
	private ApartmentService apartmentService;
	@Autowired  
	private ProjectService projectService;
	@Autowired  
	private RegionService regionService;
	@Autowired  
	private BuildingService buildingService;
	@Autowired  
	private UnitService unitService;
	@Autowired  
	private ApartmentShapeService apartmentShapeService;
	@Autowired  
	private ParameterService parameterService;
	@Autowired  
	private ClientService clientService;
	
	Logger logger = Logger.getLogger(ApartmentController.class);
	
	@RequestMapping("/sale_manage_region.do")
	public String sale_manage_region(int project_id,Model model) throws Exception {
		
		Project project = projectService.get(project_id);
		List<Region> regions = regionService.getByPorjectId(project_id);
		
		model.addAttribute("regions",regions);
		model.addAttribute("project",project);
		
		return "sales/apartment/sale_manage_region";
	}
	
	@RequestMapping("/sale_manage_building.do")
	public String sale_manage_building(int region_id ,Model model) throws Exception {
		
		List buildings = buildingService.getByRegionId(region_id);
		Region region = regionService.get(region_id);
		Project project = projectService.get(region.getProject_id());
		model.addAttribute("region_id",region_id);
		model.addAttribute("project",project);
		model.addAttribute("region",region);
		model.addAttribute("buildings",buildings);
		
		return "sales/apartment/sale_manage_building";
	}
	
	@RequestMapping("/sale_manage_apartment.do")
	public String sale_manage_apartment(int building_id ,Model model) throws Exception {
		
		Apartment apartment = new Apartment();
		Apartment apartment_unit = new Apartment();
		Unit unit = new Unit();
		Building building = buildingService.get(building_id);
		Region region = regionService.get(building.getRegion_id());
		Project project = new Project();
		
		
		
		//中间数据
		List<Apartment> unitApartment_list = new ArrayList<Apartment>(); 
		
		Map<String,Apartment> shape_apartment = new HashMap<String,Apartment>();	//房屋和户型关系
		List<Apartment> apartments = apartmentService.findApartmentByBuildingId(building_id);
		List<Unit> units = unitService.getByBuildingId(building_id);
		
		//制作房屋map
		for(int i=0; i<units.size(); i++){
			unit=units.get(i);
			unitApartment_list = new ArrayList<Apartment>();
			for(int j=0; j<apartments.size(); j++){
				apartment = apartments.get(j);
				if(apartment.getUnit_id() == unit.getId()){
					unitApartment_list.add(apartment);
				}
			}
			for(int h=0; h<unit.getFloor_total(); h++){
				for(int k=0; k<unitApartment_list.size(); k++){
					apartment_unit = unitApartment_list.get(k);
					if(apartment_unit.getFloor()==(h+1)){
						shape_apartment.put(unit.getUnit_num()+"_"+(h+1)+"_"+apartment_unit.getApartmentShape_id(), apartment_unit);
					}
				}
			}
			if(i==0){
				int project_id = apartment_unit.getProject_id();
				project = projectService.get(project_id);
			}
		}
		
		//制作用于控制显得的循环
		List<Unit_Apartmentshape> ua_list = new ArrayList<Unit_Apartmentshape>();
		
		List<String> uas = new ArrayList<String>();
		List<List> floors = new ArrayList<List>();
		Map<Integer,List> unit_ua_map = new HashMap<Integer,List>();
		List<String> shapes = new ArrayList<String>();
		for(int i=unit.getFloor_total(); i>0; i--){
			uas = new ArrayList<String>();
			for(int j=0; j<units.size(); j++){
				ua_list = unitService.getUaByUnitId(units.get(j).getId());
				unit_ua_map.put(units.get(j).getId(), ua_list);
				for(int k=0; k<ua_list.size(); k++){
					uas.add(units.get(j).getUnit_num()+"_"+String.valueOf(i)+"_"+ua_list.get(k).getApartmentShape().getId());
					if(i==1){
						shapes.add(ua_list.get(k).getApartmentShape().getUnit_type());
					}
				}
			}
			floors.add(uas);
		}
		
		model.addAttribute("unit_room_map",shape_apartment);
		model.addAttribute("floors",floors);
		model.addAttribute("units",units);
		model.addAttribute("unit_ua_map",unit_ua_map);
		model.addAttribute("shapes",shapes);
		model.addAttribute("project",project);
		model.addAttribute("region",region);
		model.addAttribute("building",building);
		
		return "sales/apartment/sale_manage_apartment";
	}
	
	@RequestMapping("/show_apartment.do")
	public String show_apartment(int apartment_id, Model model) throws Exception {
		
		Apartment apartment = apartmentService.getbyId(apartment_id);
		Project project = projectService.get(apartment.getProject_id());
		Region region = regionService.get(apartment.getRegion_id());
		Building building = buildingService.get(apartment.getBuilding_id());
		Unit unit = unitService.get(apartment.getUnit_id());
		ApartmentShape apartmentShape = apartmentShapeService.get(apartment.getApartmentShape_id());
		Client client = clientService.get(apartment.getClient_id());
		List price_reveiwing_list = apartmentService.findPriceByApartmentId(apartment_id,ApartmentPrice.STATUS_REVIEWING);
		List price_all_list = apartmentService.findPriceAllByApartmentId(apartment_id);
		
		
		List params_buildtype = parameterService.findParasByType("001");
		List params_finish = parameterService.findParasByType("002");
		List params_sale_status = parameterService.findParasByType("003");
		
		
		model.addAttribute("apartment",apartment);
		model.addAttribute("project",project);
		model.addAttribute("region",region);
		model.addAttribute("building",building);
		model.addAttribute("unit",unit);	
		model.addAttribute("apartmentShape",apartmentShape);
		model.addAttribute("client",client);
		model.addAttribute("params_buildtype", params_buildtype);
		model.addAttribute("params_finish", params_finish);
		model.addAttribute("params_sale_status", params_sale_status);
		if(price_reveiwing_list != null){
			model.addAttribute("price_reveiwing", "1");
		}
		else{
			model.addAttribute("price_reveiwing", "0");
		}
		model.addAttribute("price_list", price_all_list);
		
		return "sales/apartment/show_apartment";
	}
	
	@RequestMapping(value="/save_apartment.do", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, String> save_apartment(@RequestBody Apartment apartment, Model model) throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		
		Apartment a = apartmentService.getbyId(apartment.getId());
		a.setSale_status(apartment.getSale_status());
		a.setBuild_space(apartment.getBuild_space());
		a.setInside_space(apartment.getInside_space());
		a.setIsHaveModel(apartment.getIsHaveModel());
		if(apartment.getClient_id()!=0){
			a.setClient_id(apartment.getClient_id());
		}
		try {
			List reviewing_list = apartmentService.findPriceByApartmentId(apartment.getId(), ApartmentPrice.STATUS_REVIEWING);
			if(reviewing_list!=null && reviewing_list.size()>0){
				map.put("flag", "2");
				return map;
			}
			apartmentService.update(a);
			map.put("flag", "1");
		} catch (Exception e) {
			logger.error(e);
			map.put("flag", "0");
		}
		
		return map;
	}
	
	@RequestMapping(value="/save_price.do", method = RequestMethod.POST)
	@ResponseBody 
	public Map<String, String> save_price(@RequestBody Apartment apartment) {
		Map<String, String> map = new HashMap<String, String>();
		
		try {
			List reviewing_list = apartmentService.findPriceByApartmentId(apartment.getId(), ApartmentPrice.STATUS_REVIEWING);
			if(reviewing_list!=null && reviewing_list.size()>0){
				map.put("flag", "2");
				return map;
			}
			Apartment a = apartmentService.getbyId(apartment.getId());
			a.setUnit_price(apartment.getUnit_price());
			a.setTotal_price(apartment.getTotal_price());
			apartmentService.update(a);
			List l = apartmentService.findPriceByApartmentId(apartment.getId(), ApartmentPrice.STATUS_APPLY);
			if(l!=null && l.size()>0){
				ApartmentPrice ap =  (ApartmentPrice)l.get(0);
				apartmentService.updatePriceStatus(ap.getId(), ApartmentPrice.STATUS_STOP);
			}
			ApartmentPrice apartmentPrice = new ApartmentPrice();
			apartmentPrice.setApartment_id(apartment.getId());
			apartmentPrice.setTotal_price(apartment.getTotal_price());
			apartmentPrice.setUnit_price(apartment.getUnit_price());
			apartmentPrice.setCreate_date(new Date());
			apartmentPrice.setCreate_user_id(11);
			apartmentPrice.setStatus(2);
			apartmentService.addApartmentPrice(apartmentPrice);
			map.put("flag", "1");
		} catch (Exception e) {
			logger.error(e);
			map.put("flag", "0");
		}
		return map;
	}
	
	@RequestMapping(value = "/check_price.do", method = RequestMethod.GET)
	@ResponseBody 
	public Map<String, String> check_price(int apartment_id) throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		List l = apartmentService.findPriceByApartmentId(apartment_id,2);
		if(l==null || l.size()==0){
			map.put("flag", "1");
		}
		else{
			map.put("flag", "0");
		}
		return map;
	}
	
	@RequestMapping("/show_all_price.do")
	public String show_all_price(int apartment_id, Model model) throws Exception {
		
		Apartment apartment = apartmentService.getbyId(apartment_id);
		Project project = projectService.get(apartment.getProject_id());
		Region region = regionService.get(apartment.getRegion_id());
		Building building = buildingService.get(apartment.getBuilding_id());
		Unit unit = unitService.get(apartment.getUnit_id());
		ApartmentShape apartmentShape = apartmentShapeService.get(apartment.getApartmentShape_id());
		List price_all_list = apartmentService.findPriceAllByApartmentId(apartment_id);
		
		
		model.addAttribute("apartment",apartment);
		model.addAttribute("project",project);
		model.addAttribute("region",region);
		model.addAttribute("building",building);
		model.addAttribute("unit",unit);	
		model.addAttribute("apartmentShape",apartmentShape);
		model.addAttribute("price_list", price_all_list);
		
		return "sales/apartment/show_all_price";
	}
	
	@RequestMapping(value = "/approve_price.do", method = RequestMethod.GET)
	@ResponseBody 
	public Map<String, String> approve_price(int id) throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		try {
			apartmentService.updatePriceStatus(id,ApartmentPrice.STATUS_APPLY);
			map.put("flag", "1");
		} catch (Exception e) {
			logger.error(e);
			map.put("flag", "0");
		}
		return map;
	}
	
}