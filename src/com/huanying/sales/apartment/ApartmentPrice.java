package com.huanying.sales.apartment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ApartmentPrice implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7996859289831101346L;
	public static final int STATUS_STOP = 0;
	public static final int STATUS_APPLY = 1;
	public static final int STATUS_REVIEWING = 2;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int apartment_id;		//房屋id
	private BigDecimal unit_price;	//单价
	private BigDecimal total_price;	//总价
	
	private Date create_date;		//导入日期
	private Date execute_date;		//生效日期
	private int create_user_id;		//导入人id
	private int approve_user_id;	//审批人id
	private int status;				//状态    0停用  1正常  2等待审核（一个房屋只能有    一个等待审批的价格或一个状态正常的价格;  房屋表中的价格永远与这个价格相等）
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getApartment_id() {
		return apartment_id;
	}
	public void setApartment_id(int apartment_id) {
		this.apartment_id = apartment_id;
	}
	public BigDecimal getUnit_price() {
		return unit_price;
	}
	public void setUnit_price(BigDecimal unit_price) {
		this.unit_price = unit_price;
	}
	public BigDecimal getTotal_price() {
		return total_price;
	}
	public void setTotal_price(BigDecimal total_price) {
		this.total_price = total_price;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public Date getExecute_date() {
		return execute_date;
	}
	public void setExecute_date(Date execute_date) {
		this.execute_date = execute_date;
	}
	public int getCreate_user_id() {
		return create_user_id;
	}
	public void setCreate_user_id(int create_user_id) {
		this.create_user_id = create_user_id;
	}
	public int getApprove_user_id() {
		return approve_user_id;
	}
	public void setApprove_user_id(int approve_user_id) {
		this.approve_user_id = approve_user_id;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

}
