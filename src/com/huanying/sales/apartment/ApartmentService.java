package com.huanying.sales.apartment;

import java.util.List;

public interface ApartmentService {
	public void add(Apartment as) throws Exception;
	public void update(Apartment as) throws Exception;
	public void delete(Apartment as) throws Exception;
	public Apartment getbyId(int id) throws Exception;
	public void createByBuildingId(int building_id) throws Exception;
	public List<Apartment> findApartmentByBuildingId(int building_id) throws Exception;
	public void clearByBuildingId(int building_id) throws Exception;
	public void addApartmentPrice(ApartmentPrice ap)throws Exception;
	public List findPriceByApartmentId(int apartment_id,int status)throws Exception;
	public List findPriceAllByApartmentId(int apartment_id) throws Exception;
	public void updatePriceStatus(int id,int status) throws Exception;
}
