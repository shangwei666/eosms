package com.huanying.sales.apartment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.huanying.framework.BaseDao;
import com.huanying.sales.apartmentshape.ApartmentShape;
import com.huanying.sales.building.Building;
import com.huanying.sales.region.Region;
import com.huanying.sales.unit.Unit;
import com.huanying.sales.unit.Unit_Apartmentshape;

@Service
public class ApartmentServiceImpl implements ApartmentService {
	
	@Autowired
	private BaseDao dao;

	@Override
	public void add(Apartment apartment) throws Exception {
		
		dao.save(apartment);
	}

	@Override
	public void update(Apartment apartment) throws Exception {

		dao.update(apartment);
	}

	@Override
	public void delete(Apartment apartment) throws Exception {

		dao.delete(apartment);
	}

	@Override
	public void createByBuildingId(int building_id) throws Exception {
		
		Apartment apartment = new Apartment();
		List<Unit_Apartmentshape> ua_list = new ArrayList<Unit_Apartmentshape>();
		apartment = new Apartment();
		ApartmentShape apartmentShape = new ApartmentShape();
		Unit_Apartmentshape unit_Apartmentshape = new Unit_Apartmentshape();
		
		//获得项目、分区、楼宇信息
		Building building = (Building)dao.load(Building.class, building_id);
		int region_id = building.getRegion_id();
		Region region = (Region)dao.load(Region.class, building.getId());
		int project_id = region.getProject_id();
		
		//获得该楼各单元信息
		String hql = "from Unit where building_id=:building_id";
		Unit unit= new Unit();
		unit.setBuilding_id(building_id);
		List<Unit> unit_list = dao.exculeHql(hql, unit);
		
		if(unit_list!=null && unit_list.size()>0){
			for(int i=0;i<unit_list.size();i++){
				unit = unit_list.get(i);
				//获得单元中各户型
				hql = "from Unit_Apartmentshape where unit_id=:unit_id";
				Unit_Apartmentshape ua = new Unit_Apartmentshape();
				ua.setUnit_id(unit.getId());
				ua_list = dao.exculeHql(hql, ua);
				for(int j=0;j<ua_list.size();j++){
					unit_Apartmentshape = ua_list.get(j); 
					apartmentShape = (ApartmentShape)dao.load(ApartmentShape.class, unit_Apartmentshape.getApartmentshape_id());
					ua_list.get(j).setApartmentShape(apartmentShape);
				}
				//循环每单元楼层，根据户型生成房间
				String room_name="";
				for(int k=0; k<unit.getFloor_total(); k++){
					//循环户型
					for(int h=0; h<ua_list.size(); h++){
						unit_Apartmentshape = ua_list.get(h);						
						room_name = Integer.valueOf(k+1).toString()+"0"+unit_Apartmentshape.getRoom_num();
						apartment.setProject_id(project_id);
						apartment.setRegion_id(region_id);
						apartment.setBuilding_id(building_id);
						apartment.setUnit_id(unit.getId());
						apartment.setApartmentShape_id(unit_Apartmentshape.getApartmentshape_id());
						apartment.setName(room_name);
						apartment.setFloor(k+1);
						apartment.setBuild_space(unit_Apartmentshape.getApartmentShape().getBuild_space());
						apartment.setInside_space(unit_Apartmentshape.getApartmentShape().getInside_space());
						apartment.setIsHaveModel(1);
						apartment.setSale_status(1);
						apartment.setStatus(1);
						dao.save(apartment);
					}
				}
			}
		}
	}

	@Override
	public List<Apartment> findApartmentByBuildingId(int building_id) throws Exception {
		String hql="from Apartment where building_id=:building_id and status=1";
		Apartment apartment = new Apartment();
		apartment.setBuilding_id(building_id);
		List<Apartment> l = dao.exculeHql(hql, apartment);
		return l;
	}

	@Override
	public void clearByBuildingId(int building_id) throws Exception {
		//清除该楼所有房间
		String hql = "update Apartment set status=0 where building_id=:building_id";
		Apartment apartment = new Apartment();
		apartment.setBuilding_id(building_id);
		dao.excule(hql, apartment);
	}

	@Override
	public Apartment getbyId(int id) throws Exception {
		return (Apartment)dao.load(Apartment.class, id);
	}
	
	@Override
	public void addApartmentPrice(ApartmentPrice ap)throws Exception {
		dao.save(ap);
	}
	
	@Override
	public List findPriceByApartmentId(int apartment_id,int status)throws Exception {
		String hql = "from ApartmentPrice where apartment_id=:apartment_id and status=:status";
		ApartmentPrice apartmentPrice = new ApartmentPrice();
		apartmentPrice.setApartment_id(apartment_id);
		apartmentPrice.setStatus(status);
		List l = dao.exculeHql(hql, apartmentPrice);
		if(l.size()>0){
			return l;
		}
		else{
			return null;
		}
	}

	@Override
	public List findPriceAllByApartmentId(int apartment_id) throws Exception {
		String hql = "from ApartmentPrice where apartment_id=:apartment_id order by id desc";
		ApartmentPrice apartmentPrice = new ApartmentPrice();
		apartmentPrice.setApartment_id(apartment_id);
		List l = dao.exculeHql(hql, apartmentPrice);
		if(l.size()>0){
			return l;
		}
		else{
			return null;
		}
	}
	
	public void updatePriceStatus(int id,int status) throws Exception{
		String hql;
		if(status == ApartmentPrice.STATUS_APPLY){
			hql = "update ApartmentPrice set status=:status,execute_date=:execute_date where id=:id";
		}
		else{
			hql = "update ApartmentPrice set status=:status where id=:id";
		}
		ApartmentPrice apartmentPrice = new ApartmentPrice();
		apartmentPrice.setExecute_date(new Date());
		apartmentPrice.setId(id);
		apartmentPrice.setStatus(status);
		dao.excule(hql, apartmentPrice);	
	}
}
