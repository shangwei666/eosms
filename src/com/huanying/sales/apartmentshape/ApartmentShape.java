package com.huanying.sales.apartmentshape;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;

import com.huanying.framework.parameter.Parameter;

@Entity
public class ApartmentShape implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8164934023091412046L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int project_id;			//项目id
	private String code;			//户型编号
	private String unit_type;		//户型结构
	private String build_space;		//建筑面积   
	private String inside_space;	//套内面积
	private String build_type;		//建筑类型
	private String decoration;		//装修标准
	@Column(name="status",nullable=false,columnDefinition="INT default 1")
	private int status;
	@Transient
	private Parameter para_bulid_type;		//建筑类型
	@Transient
	private Parameter para_decoration;		//装修标准
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getProject_id() {
		return project_id;
	}
	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getUnit_type() {
		return unit_type;
	}
	public void setUnit_type(String unit_type) {
		this.unit_type = unit_type;
	}
	public String getBuild_space() {
		return build_space;
	}
	public void setBuild_space(String build_space) {
		this.build_space = build_space;
	}
	public String getInside_space() {
		return inside_space;
	}
	public void setInside_space(String inside_space) {
		this.inside_space = inside_space;
	} 
	public String getBuild_type() {
		return build_type;
	}
	public void setBuild_type(String build_type) {
		this.build_type = build_type;
	}
	public String getDecoration() {
		return decoration;
	}
	public void setDecoration(String decoration) {
		this.decoration = decoration;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	@OneToOne(cascade = CascadeType.ALL)  
	@PrimaryKeyJoinColumn
	public Parameter getPara_bulid_type() {
		return para_bulid_type;
	}
	public void setPara_bulid_type(Parameter para_bulid_type) {
		this.para_bulid_type = para_bulid_type;
	}
	@OneToOne(cascade = CascadeType.ALL)  
	@PrimaryKeyJoinColumn
	public Parameter getPara_decoration() {
		return para_decoration;
	}
	public void setPara_decoration(Parameter para_decoration) {
		this.para_decoration = para_decoration;
	}
	
	
}
