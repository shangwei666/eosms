package com.huanying.sales.apartmentshape;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.huanying.framework.BaseController;
import com.huanying.framework.PageBean;
import com.huanying.framework.parameter.Parameter;
import com.huanying.framework.parameter.ParameterService;
import com.huanying.sales.region.Region;

@Controller
public class ApartmentShapeController extends BaseController {
	@Autowired  
	private ApartmentShapeService apartmentShapeService;
	@Autowired  
	private ParameterService parameterService;
	
	Logger logger = Logger.getLogger(ApartmentShapeController.class);
	
	@RequestMapping("/show_add_apartmentShape.do")
	public String show_add(String id, String project_id,Model model) throws Exception {
		int pro_id = Integer.valueOf(project_id);
		List apartmentShapes = apartmentShapeService.findApartmentShapeByProjectId(pro_id);
		model.addAttribute("apartmentShapes", apartmentShapes);
		
		List params_buildtype = parameterService.findParasByType(Parameter.BUILD_TYPE);
		model.addAttribute("params_buildtypes", params_buildtype);
		List params_finish = parameterService.findParasByType(Parameter.FITMENT_TYPE);
		model.addAttribute("params_finishs", params_finish);
		
		model.addAttribute("project_id", project_id);
		
		return "sales/apartmentshape/add_apartmentshape";
	}
	
	@RequestMapping(value="/save_apartmentShape.do", method = RequestMethod.POST)
	@ResponseBody 
	public Map<String, String> save(@RequestBody ApartmentShape as) {
		Map<String, String> map = new HashMap<String, String>();
		ApartmentShape a;
		try {
			if(as.getId()==0){
				a = apartmentShapeService.add(as);
			}
			else{
				apartmentShapeService.update(as);
				a = as;
			}
			map.put("flag", "1");
		} catch (Exception e) {
			logger.error("操作户型失败",e);
			map.put("flag", "0");
		}
		return map;
	}
	
	@RequestMapping(value = "/delete_apartmentShape.do", method = RequestMethod.POST)
	@ResponseBody 
	public Map<String, String> delete(@RequestBody ApartmentShape as) throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		try {
			if(as.getId()!=0){
				apartmentShapeService.delete(as);
				map.put("flag", "1");
			}
			else{
				map.put("flag", "2");  //未发现需要删除数据
			}
		} catch (Exception e) {
			logger.error("操作分区失败",e);
			map.put("flag", "0");
		}
		return map;
	}

}