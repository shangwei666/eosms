package com.huanying.sales.apartmentshape;

import java.util.List;

import com.huanying.framework.PageBean;

public interface ApartmentShapeService {
	public ApartmentShape add(ApartmentShape as) throws Exception;
	
	public void update(ApartmentShape as) throws Exception;
	
	public void delete(ApartmentShape as) throws Exception;
	
	public ApartmentShape get(int id) throws Exception;
	
	public PageBean findApartmentShape (int pageSize,int page) throws Exception;
	
	public List<ApartmentShape> findApartmentShapeByUnitId(int unit_id) throws Exception;
	
	public List<ApartmentShape> findApartmentShapeByProjectId(int project_id) throws Exception;
}
