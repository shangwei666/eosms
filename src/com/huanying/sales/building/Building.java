package com.huanying.sales.building;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Building implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9008976051374957973L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String building_num;		//楼编号
	private int region_id;		//分区id
	@Column(name="status",nullable=false,columnDefinition="INT default 1")
	private int status;			//状态    0删除   1正常
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBuilding_num() {
		return building_num;
	}
	public void setBuilding_num(String building_num) {
		this.building_num = building_num;
	}
	public int getRegion_id() {
		return region_id;
	}
	public void setRegion_id(int region_id) {
		this.region_id = region_id;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

}
