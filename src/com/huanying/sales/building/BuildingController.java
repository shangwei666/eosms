package com.huanying.sales.building;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.huanying.framework.BaseController;
import com.huanying.sales.apartment.ApartmentService;

@Controller
public class BuildingController extends BaseController {
	@Autowired  
	private BuildingService buildingService;
	@Autowired  
	private ApartmentService apartmentService;
	
	Logger logger = Logger.getLogger(BuildingController.class);
	
	@RequestMapping("/show_building.do")
	public String show_add(int region_id ,Model model) throws Exception {
		
		List buildings = buildingService.getByRegionId(region_id);
		
		model.addAttribute("region_id",region_id);
		model.addAttribute("buildings",buildings);
		
		return "sales/building/add_building";
	}
	
	
	@RequestMapping(value = "/add_building.do", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, String> add_building(@RequestBody Building building) throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		try {
			if(building.getId()==0){
				buildingService.add(building);
			}
			else{
				buildingService.update(building);
			}
			map.put("flag", "1");
			map.put("id",String.valueOf(building.getId()));
		} catch (Exception e) {
			logger.error("操作楼宇失败",e);
			map.put("flag", "0");
		}
		
		return map;
	}
	
	
	@RequestMapping(value = "/delete_building.do", method = RequestMethod.POST)
	@ResponseBody 
	public Map<String, String> delete_building(@RequestBody Building building) throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		try {
			if(building.getId()!=0){
				building.setStatus(0);
				buildingService.delete(building);
				map.put("flag", "1");
			}
			else{
				map.put("flag", "2");  //未发现需要删除数据
			}
		} catch (Exception e) {
			logger.error("操作分区失败",e);
			map.put("flag", "0");
		}
		return map;
	}
	
	@RequestMapping(value = "/create_apartment.do", method = RequestMethod.POST)
	@ResponseBody 
	public Map<String, String> create_apartment(int building_id) throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		try {
			if(building_id!=0){
				apartmentService.createByBuildingId(building_id);
				map.put("flag", "1");
			}
			else{
				map.put("flag", "2");  //未发现需要删除数据
			}
		} catch (Exception e) {
			logger.error("创建房间失败",e);
			map.put("flag", "0");
		}
		return map;
	}
	
	@RequestMapping(value = "/clear_apartment.do", method = RequestMethod.POST)
	@ResponseBody 
	public Map<String, String> clear_apartment(int building_id) throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		try {
			if(building_id!=0){
				apartmentService.clearByBuildingId(building_id);
				map.put("flag", "1");
			}
			else{
				map.put("flag", "2");  //未发现需要删除数据
			}
		} catch (Exception e) {
			logger.error("清空房间失败",e);
			map.put("flag", "0");
		}
		return map;
	}

}
