package com.huanying.sales.building;

import java.util.List;

public interface BuildingService {
	
	public void add(Building building) throws Exception;
	
	public void update(Building building) throws Exception;
	
	public void delete(Building building) throws Exception;
	
	public Building get(int id) throws Exception;
	
	public List getByRegionId(int id) throws Exception;
	
}
