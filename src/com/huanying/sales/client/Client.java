package com.huanying.sales.client;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Client implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6635738019985898149L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String name;		//姓名
	private String phone;		//电话
	private String show_phone;	//显示用电话  格式 188****8888
	private String sex;			//性别
	private String visit_date;	//来访日期
	private String hope_shape;	//需求户型
	private String hope_price;	//需求单价
	private String hope_floor;	//需求楼层
	private String focus;		//关注点
	private String sales;		//置业顾问
	private String sales_company;	//经纪公司
	
	
	private String visit_type;	//来客类型
	private String pay_type;	//支付方式
	private String use_type;	//购房用途
	private String know_type;	//认知途径
	private String job_type;	//职业
	private String family;		//家庭构成
	private String client_area;	//客户区域
	
	private Date input_date;	//录入时间
	
	@Column(name="status",nullable=false,columnDefinition="INT default 1")
	private int status;			//状态    0删除   1正常

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getShow_phone() {
		return show_phone;
	}

	public void setShow_phone(String show_phone) {
		this.show_phone = show_phone;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getVisit_date() {
		return visit_date;
	}

	public void setVisit_date(String visit_date) {
		this.visit_date = visit_date;
	}

	public String getHope_shape() {
		return hope_shape;
	}

	public void setHope_shape(String hope_shape) {
		this.hope_shape = hope_shape;
	}

	public String getHope_price() {
		return hope_price;
	}

	public void setHope_price(String hope_price) {
		this.hope_price = hope_price;
	}

	public String getHope_floor() {
		return hope_floor;
	}

	public void setHope_floor(String hope_floor) {
		this.hope_floor = hope_floor;
	}

	public String getFocus() {
		return focus;
	}

	public void setFocus(String focus) {
		this.focus = focus;
	}

	public String getSales() {
		return sales;
	}

	public void setSales(String sales) {
		this.sales = sales;
	}

	public String getSales_company() {
		return sales_company;
	}

	public void setSales_company(String sales_company) {
		this.sales_company = sales_company;
	}

	public String getVisit_type() {
		return visit_type;
	}

	public void setVisit_type(String visit_type) {
		this.visit_type = visit_type;
	}

	public String getPay_type() {
		return pay_type;
	}

	public void setPay_type(String pay_type) {
		this.pay_type = pay_type;
	}

	public String getUse_type() {
		return use_type;
	}

	public void setUse_type(String use_type) {
		this.use_type = use_type;
	}

	public String getKnow_type() {
		return know_type;
	}

	public void setKnow_type(String know_type) {
		this.know_type = know_type;
	}

	public String getJob_type() {
		return job_type;
	}

	public void setJob_type(String job_type) {
		this.job_type = job_type;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getClient_area() {
		return client_area;
	}

	public void setClient_area(String client_area) {
		this.client_area = client_area;
	}

	public Date getInput_date() {
		return input_date;
	}

	public void setInput_date(Date input_date) {
		this.input_date = input_date;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
}
