package com.huanying.sales.client;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.huanying.framework.BaseController;
import com.huanying.framework.PageBean;
import com.huanying.framework.parameter.Parameter;
import com.huanying.framework.parameter.ParameterService;
import com.huanying.sales.apartmentshape.ApartmentShape;
import com.huanying.sales.apartmentshape.ApartmentShapeController;

@Controller
public class ClientController extends BaseController {
	@Autowired
	private ClientService clientService;
	@Autowired  
	private ParameterService parameterService;
	
	Logger logger = Logger.getLogger(ClientController.class);
	
	@RequestMapping("/show_client.do")
	public String show_client(String porject_id,String page_num,String name,String show_phone, Model model) throws Exception {
		
		if(page_num ==null){
			page_num = "1";
		}
		PageBean pageBean = clientService.searchClients(name, show_phone, 3, Integer.valueOf(page_num));
		
		List params_visitType = parameterService.findParasByType(Parameter.VISIT_TYPE);
		model.addAttribute("params_visitType", params_visitType);
		List params_payType = parameterService.findParasByType(Parameter.PAY_TYPE);
		model.addAttribute("params_payType", params_payType);
		List params_useType = parameterService.findParasByType(Parameter.USE_TYPE);
		model.addAttribute("params_useType", params_useType);
		List params_knowType = parameterService.findParasByType(Parameter.KNOW_TYPE);
		model.addAttribute("params_knowType", params_knowType);
		List params_jobType = parameterService.findParasByType(Parameter.JOB_TYPE);
		model.addAttribute("params_jobType", params_jobType);
		List params_family = parameterService.findParasByType(Parameter.FAMILY);
		model.addAttribute("params_family", params_family);
		List params_clientArea = parameterService.findParasByType(Parameter.CLIENT_AREA);
		model.addAttribute("params_clientArea", params_clientArea);
		List params_foucsType = parameterService.findParasByType(Parameter.FOUCS_TYPE);
		model.addAttribute("params_foucsType", params_foucsType);
		List params_hopeShape = parameterService.findParasByType(Parameter.HOPE_SHAPE);
		model.addAttribute("params_hopeShape", params_hopeShape);
		
		model.addAttribute("clients", pageBean.getList());
		model.addAttribute("page",pageBean);
		model.addAttribute("name", name);
		model.addAttribute("show_phone", show_phone);
		
		return "sales/client/add_client";
	}
	
	@RequestMapping(value="/add_client.do", method = RequestMethod.POST)
	@ResponseBody 
	public Map<String, String> save(@RequestBody Client client) {
		Map<String, String> map = new HashMap<String, String>();
		try {
			if(client.getId()==0){
				clientService.add(client);
			}
			else{
				clientService.update(client);
			}
			map.put("flag", "1");
			map.put("id", String.valueOf(client.getId()));
		} catch (Exception e) {
			logger.error("操作客户失败",e);
			map.put("flag", "0");
		}
		return map;
	}
	
	
	@RequestMapping(value="/get_client.do", method = RequestMethod.GET)
	@ResponseBody 
	public Map<String, Object> get_client(int id) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Client c = clientService.get(id);
			map.put("flag", "1");
			map.put("client", c);
		} catch (Exception e) {
			logger.error("查询客户失败",e);
			map.put("flag", "0");
		}
		
		return map;
	}
	
	@RequestMapping(value="/search_client.do", method = RequestMethod.GET)
	@ResponseBody 
	public Map<String, Object> search_client(String phone) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Client client = clientService.getClientByPhone(phone);
			if(client != null){
				map.put("flag", "1");
				map.put("client", client);
			}
			else{
				map.put("flag", "0");
			}
		} catch (Exception e) {
			logger.error("查询客户失败",e);
			map.put("flag", "0");
		}
		
		return map;
	}
	
}
