package com.huanying.sales.client;

import com.huanying.framework.PageBean;

public interface ClientService {
	
	public void add(Client client) throws Exception;
	
	public void update(Client client) throws Exception;
	
	public void delete(Client client) throws Exception;
	
	public Client get(int id) throws Exception;
	
	public PageBean findClients(int pageSize,int page) throws Exception;
	
	public PageBean searchClients(String name,String phone,int page,int pageSize) throws Exception;
	
	public Client getClientByPhone(String phone) throws Exception;

}
