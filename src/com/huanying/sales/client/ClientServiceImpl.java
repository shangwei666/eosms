package com.huanying.sales.client;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.huanying.framework.BaseDao;
import com.huanying.framework.PageBean;

@Service
public class ClientServiceImpl implements ClientService {
	
	Logger logger = Logger.getLogger(ClientServiceImpl.class);
	
	@Autowired
	private BaseDao dao;

	@Override
	public void add(Client client) throws Exception {
		dao.save(client);
	}

	@Override
	public void update(Client client) throws Exception {
		dao.update(client);
	}

	@Override
	public void delete(Client client) throws Exception {
		dao.delete(client);
	}

	@Override
	public Client get(int id) throws Exception {
		return (Client)dao.load(Client.class, id);
	}
	
	@Override
	public PageBean findClients(int pageSize,int page) throws Exception {
		
		String hql="from Client where status=1";
		
		int allRow = dao.queryAllRowCount(hql, null);
		int totalPage = PageBean.countTotalPage(pageSize, allRow);
		final int offset = PageBean.countOffset(pageSize, page);
	    final int currentPage = PageBean.countCurrentPage(page);
	    List list = dao.queryForPageAndParams(hql,null, offset, pageSize);
	    
	    //把分页信息保存到Bean中
        PageBean pageBean = new PageBean();
        pageBean.setPageSize(pageSize);    
        pageBean.setCurrentPage(currentPage);
        pageBean.setAllRow(allRow);
        pageBean.setTotalPage(totalPage);
        pageBean.setList(list);
        pageBean.init();
		
		return pageBean;
	}
	
	@Override
	public PageBean searchClients(String name,String phone,int pageSize,int page) throws Exception{
		
		StringBuilder hql = new StringBuilder("from Client where status=1");
		List<Object> params = new ArrayList<Object>();
		if(name!=null && !("").equals(name)){
			hql.append(" and name like ?");
			params.add("%"+name+"%");
		}
		
		if(phone!=null && !("").equals(phone)){
			hql.append(" and phone like ?");
			params.add("%"+phone+"%");
		}
		
		int allRow = dao.queryAllRowCount(hql.toString(), params);
		int totalPage = PageBean.countTotalPage(pageSize, allRow);
		final int offset = PageBean.countOffset(pageSize, page);
	    final int currentPage = PageBean.countCurrentPage(page);
	    List<Object> list = dao.queryForPageAndParams(hql.toString(), params, offset, pageSize);
	    
	    //把分页信息保存到Bean中
        PageBean pageBean = new PageBean();
        pageBean.setPageSize(pageSize);    
        pageBean.setCurrentPage(currentPage);
        pageBean.setAllRow(allRow);
        pageBean.setTotalPage(totalPage);
        pageBean.setList(list);
        pageBean.init();
		
		return pageBean;
	}
	
	public Client getClientByPhone(String phone) throws Exception{
		StringBuilder hql = new StringBuilder("from Client where status=1");
		List<Object> params = new ArrayList<Object>();
		
		if(phone!=null && !("").equals(phone)){
			hql.append(" and phone=?");
			params.add(phone);
		}
	    List<Object> list = dao.queryForPageAndParams(hql.toString(), params, 0, 1);
	    
	    if(list!=null && list.size()>0){
	    	return (Client)list.get(0);
	    }
	    else{
	    	return null;
	    }
	}

}
