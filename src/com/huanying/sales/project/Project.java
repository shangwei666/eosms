package com.huanying.sales.project;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author lennonyou
 *
 */
@Entity
public class Project implements Serializable {
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;					//项目名称
	private String short_name;				//项目简称
	private String image_url;				//项目俯视图
	
	private String area;					//环境位置
	private String plot_ratio;				//容积率

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date open_date;					//开盘时间
	private float property_fee;				//物业费
	private String land_developer;			//开发商
	
	private String presell_licence; 		//预售许可证
	private String sales_offices_address;	//售楼处地址
	private String property_offices_address;//物业地址
	private String greening_rate;			//绿化率
	private String property_offices;		//物业公司
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date close_date; 				//交房时间
	private float average_price;			//项目均价
	
	private String build_type;				//建筑类型
	private String fitment;					//装修状况
	private String business_district;		//所属商圈
	private String property_type;			//物业类别
	
	@Column(name="status",nullable=false,columnDefinition="INT default 1")
	private int status;						//状态  0删除  1正常  默认值为1

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getShort_name() {
		return short_name;
	}
	public void setShort_name(String short_name) {
		this.short_name = short_name;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getPlot_ratio() {
		return plot_ratio;
	}
	public void setPlot_ratio(String plot_ratio) {
		this.plot_ratio = plot_ratio;
	}
	public Date getOpen_date() {
		return open_date;
	}
	public void setOpen_date(Date open_date) {
		this.open_date = open_date;
	}
	public float getProperty_fee() {
		return property_fee;
	}
	public void setProperty_fee(float property_fee) {
		this.property_fee = property_fee;
	}
	public String getLand_developer() {
		return land_developer;
	}
	public void setLand_developer(String land_developer) {
		this.land_developer = land_developer;
	}
	public String getPresell_licence() {
		return presell_licence;
	}
	public void setPresell_licence(String presell_licence) {
		this.presell_licence = presell_licence;
	}
	public String getSales_offices_address() {
		return sales_offices_address;
	}
	public void setSales_offices_address(String sales_offices_address) {
		this.sales_offices_address = sales_offices_address;
	}
	public String getProperty_offices_address() {
		return property_offices_address;
	}
	public void setProperty_offices_address(String property_offices_address) {
		this.property_offices_address = property_offices_address;
	}
	public String getGreening_rate() {
		return greening_rate;
	}
	public void setGreening_rate(String greening_rate) {
		this.greening_rate = greening_rate;
	}
	public String getProperty_offices() {
		return property_offices;
	}
	public void setProperty_offices(String property_offices) {
		this.property_offices = property_offices;
	}
	public Date getClose_date() {
		return close_date;
	}
	public void setClose_date(Date close_date) {
		this.close_date = close_date;
	}
	public float getAverage_price() {
		return average_price;
	}
	public void setAverage_price(float average_price) {
		this.average_price = average_price;
	}
	public String getBuild_type() {
		return build_type;
	}
	public void setBuild_type(String build_type) {
		this.build_type = build_type;
	}
	public String getFitment() {
		return fitment;
	}
	public void setFitment(String fitment) {
		this.fitment = fitment;
	}
	public String getBusiness_district() {
		return business_district;
	}
	public void setBusiness_district(String business_district) {
		this.business_district = business_district;
	}
	public String getProperty_type() {
		return property_type;
	}
	public void setProperty_type(String property_type) {
		this.property_type = property_type;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
}
