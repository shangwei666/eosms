package com.huanying.sales.project;

import java.io.File;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.huanying.framework.BaseController;
import com.huanying.framework.PageBean;
import com.huanying.framework.parameter.Parameter;
import com.huanying.framework.parameter.ParameterService;

@Controller
public class ProjectController extends BaseController {
	@Autowired  
	private ProjectService projectService;
	@Autowired  
	private ParameterService parameterService;
	
	Logger logger = Logger.getLogger(ProjectController.class);
	
	@RequestMapping("/show_add_project.do")
	public String show_add(@RequestParam(value = "id", required = false)String id, Model model) throws Exception {
		Project pro;
		if(id!=null && !("").equals(id)){
			 pro = projectService.get(Integer.valueOf(id));
			model.addAttribute("project", pro);
		}
		
		List params_buildType = parameterService.findParasByType(Parameter.BUILD_TYPE);
		model.addAttribute("params_buildType", params_buildType);
		List params_fitmentType = parameterService.findParasByType(Parameter.FITMENT_TYPE);
		model.addAttribute("params_fitmentType", params_fitmentType);
		List params_propertyType = parameterService.findParasByType(Parameter.PROPERTY_TYPE);
		model.addAttribute("params_propertyType", params_propertyType);
		List params_businessDistrict = parameterService.findParasByType(Parameter.BUSINESS_DISTRICT);
		model.addAttribute("params_businessDistrict", params_businessDistrict);
		
		return "sales/project/add_project";
	}
	
	@RequestMapping("/save_project.do")
	public String save(Project project, @RequestParam("file") MultipartFile file, Model model) throws Exception {
		Project pro;
		String file_name = "";
		if (!file.isEmpty()) {
            try {
            	String temp_name = file.getOriginalFilename();
            	String postfix = temp_name.substring(temp_name.lastIndexOf("."), temp_name.length());
            	file_name = UUID.randomUUID().toString()+postfix;
            	String filePath = getSession().getServletContext().getRealPath("/") + "fileUpload" + File.separator + file_name;
                logger.info(filePath);
                file.transferTo(new File(filePath));
                project.setImage_url(file_name);
                logger.info("You successfully uploaded");
            } catch (Exception e) {
            	logger.error("You failed to upload",e);
            }
        } else {
        	logger.info("You failed to upload because the file was empty.");
        }
		
		try {
			if(project.getId()==0){
				pro = projectService.add(project);
			}
			else{
				projectService.update(project);
				pro = project;
			}
			model.addAttribute("project", pro);
		} catch (Exception e) {
			logger.error("保存项目"+project.getName()+"失败",e);
		}
		
		List params_buildType = parameterService.findParasByType(Parameter.BUILD_TYPE);
		model.addAttribute("params_buildType", params_buildType);
		List params_fitmentType = parameterService.findParasByType(Parameter.FITMENT_TYPE);
		model.addAttribute("params_fitmentType", params_fitmentType);
		List params_propertyType = parameterService.findParasByType(Parameter.PROPERTY_TYPE);
		model.addAttribute("params_propertyType", params_propertyType);
		List params_businessDistrict = parameterService.findParasByType(Parameter.BUSINESS_DISTRICT);
		model.addAttribute("params_businessDistrict", params_businessDistrict);
		return "sales/project/add_project";
	}
	
	@RequestMapping("/show_project.do")
	public String show_project(Model model) throws Exception {
		PageBean pageBean = projectService.findProject(10, 1);
		model.addAttribute("projects", pageBean.getList());
		model.addAttribute("page",pageBean);
		return "sales/project/show_project";
	}
	
	public static void main(String[] args) {
		String temp_name = "dsfds.jpg";
    	String postfix = temp_name.substring(temp_name.lastIndexOf("."), temp_name.length());
    	System.out.println(postfix);
	}
}
