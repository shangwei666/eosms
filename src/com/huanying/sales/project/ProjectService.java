package com.huanying.sales.project;

import com.huanying.framework.PageBean;

public interface ProjectService {
	
	public Project add(Project project) throws Exception;
	
	public void update(Project project) throws Exception;
	
	public Project get(int id) throws Exception;
	
	public PageBean findProject (int pageSize,int page) throws Exception;
	
	public int findProjectToatl() throws Exception;
}
