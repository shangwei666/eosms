package com.huanying.sales.project;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.huanying.framework.BaseDao;
import com.huanying.framework.PageBean;
@Service
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	private BaseDao dao;
	
	@Override
	public Project add(Project project) throws Exception {
		Project pro = (Project)dao.save(project);
		return pro;
	}

	@Override
	public void update(Project project) throws Exception {
		dao.update(project);
	}

	@Override
	public PageBean findProject(int pageSize,int page) throws Exception {
		
		String hql="from Project where status=1";
		
		int allRow = dao.queryAllRowCount(hql, null);
		int totalPage = PageBean.countTotalPage(pageSize, allRow);
		final int offset = PageBean.countOffset(pageSize, page);
	    final int currentPage = PageBean.countCurrentPage(page);
	    List list = dao.queryForPageAndParams(hql,null, offset, pageSize);
	    
	    //把分页信息保存到Bean中
        PageBean pageBean = new PageBean();
        pageBean.setPageSize(pageSize);    
        pageBean.setCurrentPage(currentPage);
        pageBean.setAllRow(allRow);
        pageBean.setTotalPage(totalPage);
        pageBean.setList(list);
        pageBean.init();
		
		return pageBean;
	}

	@Override
	public Project get(int id) throws Exception {
		Project project = (Project)dao.load(Project.class, id);
		return project;
	}
	
	@Override
	public int findProjectToatl() throws Exception {
		
		String hql="from Project where status=1";
		
		return dao.queryAllRowCount(hql, null);
	}

}
