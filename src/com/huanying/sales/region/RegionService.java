package com.huanying.sales.region;

import java.util.List;

public interface RegionService {
	public void add(Region region) throws Exception;
	
	public void update(Region region) throws Exception;
	
	public int delete(Region region) throws Exception;
	
	public Region get(int id) throws Exception;
	
	public List getByPorjectId(int project_id) throws Exception;
}
