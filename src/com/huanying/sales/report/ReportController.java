package com.huanying.sales.report;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
public class ReportController {
	@RequestMapping("/report_main.do")
	public String report_main(String id, String project_id,Model model) throws Exception {
		return "sales/report/report_main";
	}
}
