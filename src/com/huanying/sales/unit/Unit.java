package com.huanying.sales.unit;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Unit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2974417238280358117L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String unit_num;	//单元编号
	private int floor_total;   //楼层总数
	private int building_id;		//楼id
	@Column(name="status",nullable=false,columnDefinition="INT default 1")
	private int status;			//状态    0删除   1正常
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUnit_num() {
		return unit_num;
	}
	public void setUnit_num(String unit_num) {
		this.unit_num = unit_num;
	}
	public int getFloor_total() {
		return floor_total;
	}
	public void setFloor_total(int floor_total) {
		this.floor_total = floor_total;
	}
	public int getBuilding_id() {
		return building_id;
	}
	public void setBuilding_id(int building_id) {
		this.building_id = building_id;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
}
