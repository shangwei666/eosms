package com.huanying.sales.unit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.huanying.framework.BaseController;
import com.huanying.sales.apartmentshape.ApartmentShape;
import com.huanying.sales.apartmentshape.ApartmentShapeService;

@Controller
public class UnitController extends BaseController {
	
	@Autowired  
	private UnitService unitService;
	
	@Autowired  
	private ApartmentShapeService apartmentShapeService;
	
	Logger logger = Logger.getLogger(UnitController.class);
	
	@RequestMapping("/show_add_unit.do")
	public String show_add(int building_id ,Model model) throws Exception {
		
		List<Unit> units = unitService.getByBuildingId(building_id);
		model.addAttribute("units",units);
		model.addAttribute("building_id",building_id);
		
		return "sales/building/add_unit";
	}
	
	
	@RequestMapping(value = "/add_unit.do", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, String> add_unit(@RequestBody Unit unit) throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		try {
			if(unit.getId()==0){
				unitService.add(unit);
			}
			else{
				unitService.update(unit);
			}
			map.put("flag", "1");
			map.put("id",String.valueOf(unit.getId()));
		} catch (Exception e) {
			logger.error("操作单元失败",e);
			map.put("flag", "0");
		}
		
		return map;
	}
	
	
	@RequestMapping(value = "/delete_unit.do", method = RequestMethod.POST)
	@ResponseBody 
	public Map<String, String> delete_unit(@RequestBody Unit unit) throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		try {
			if(unit.getId()!=0){
				unit.setStatus(0);
				unitService.delete(unit);
				map.put("flag", "1");
			}
			else{
				map.put("flag", "2");  //未发现需要删除数据
			}
		} catch (Exception e) {
			logger.error("操作单元失败",e);
			map.put("flag", "0");
		}
		return map;
	}
	
	@RequestMapping("/show_add_ua.do")
	public String show_add_ua(int unit_id ,Model model) throws Exception {
		
		List<ApartmentShape> apartmentShapes = apartmentShapeService.findApartmentShapeByUnitId(unit_id);
		List<Unit_Apartmentshape> uas = unitService.getUaByUnitId(unit_id);
		model.addAttribute("apartmentShapes",apartmentShapes);
		model.addAttribute("uas",uas);
		model.addAttribute("unit_id",unit_id);
		
		return "sales/building/add_ua";
	}
	
	@RequestMapping(value = "/add_ua.do", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, String> add_ua(@RequestBody Unit_Apartmentshape ua) throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		try {
			if(ua.getId()==0){
				unitService.add_ua(ua);
			}
			else{
				unitService.update_ua(ua);
			}
			map.put("flag", "1");
			map.put("id",String.valueOf(ua.getId()));
		} catch (Exception e) {
			logger.error("挂接户型失败",e);
			map.put("flag", "0");
		}
		
		return map;
	}
	
	@RequestMapping(value = "/delete_ua.do", method = RequestMethod.POST)
	@ResponseBody 
	public Map<String, String> delete_ua(@RequestBody Unit_Apartmentshape ua) throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		try {
			if(ua.getId()!=0){
				ua.setStatus(0);
				unitService.delete_ua(ua);
				map.put("flag", "1");
			}
			else{
				map.put("flag", "2");  //未发现需要删除数据
			}
		} catch (Exception e) {
			logger.error("删除挂机户型失败",e);
			map.put("flag", "0");
		}
		return map;
	}

}
