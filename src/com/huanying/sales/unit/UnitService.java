package com.huanying.sales.unit;

import java.util.List;

public interface UnitService {
	
	public void add(Unit unit) throws Exception;
	
	public void update(Unit unit) throws Exception;
	
	public void delete(Unit unit) throws Exception;
	
	public Unit get(int id) throws Exception;
	
	public List<Unit> getByBuildingId(int id) throws Exception;
	
	public void add_ua(Unit_Apartmentshape ua) throws Exception;
	
	public void update_ua(Unit_Apartmentshape ua) throws Exception;
	
	public void delete_ua(Unit_Apartmentshape ua) throws Exception;
	
	public List<Unit_Apartmentshape> getUaByUnitId(int unit_id) throws Exception;
}
