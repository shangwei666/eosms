package com.huanying.sales.unit;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.huanying.framework.BaseDao;
import com.huanying.sales.apartmentshape.ApartmentShape;
import com.huanying.sales.building.Building;
import com.huanying.sales.region.RegionServiceImpl;

@Service
public class UnitServiceImpl implements UnitService {
	Logger logger = Logger.getLogger(UnitServiceImpl.class);
	
	@Autowired
	private BaseDao dao;

	@Override
	public void add(Unit unit) throws Exception {
		dao.save(unit);
	}

	@Override
	public void update(Unit unit) throws Exception {
		dao.update(unit);
	}

	@Override
	public void delete(Unit unit) throws Exception {
		dao.delete(unit);
	}

	@Override
	public Unit get(int id) throws Exception {
		return (Unit)dao.load(Unit.class, id);
	}

	@Override
	public List<Unit> getByBuildingId(int id) throws Exception {
		String hql="from Unit where building_id=:building_id and status=1";
		Unit u = new Unit();
		u.setBuilding_id(id);
		List<Unit> l = dao.exculeHql(hql, u);
		return l;
	}

	@Override
	public void add_ua(Unit_Apartmentshape ua) throws Exception {
		dao.save(ua);
		
	}

	@Override
	public void update_ua(Unit_Apartmentshape ua) throws Exception {
		dao.update(ua);
		
	}

	@Override
	public void delete_ua(Unit_Apartmentshape ua) throws Exception {
		dao.delete(ua);
		
	}

	@Override
	public List<Unit_Apartmentshape> getUaByUnitId(int unit_id) throws Exception {
		String hql = "from Unit_Apartmentshape where unit_id=:unit_id";
		Unit_Apartmentshape ua = new Unit_Apartmentshape();
		ua.setUnit_id(unit_id);
		List<Unit_Apartmentshape> l = dao.exculeHql(hql, ua);
		for(int i=0;i<l.size();i++){
			ApartmentShape a = (ApartmentShape)dao.load(ApartmentShape.class, l.get(i).getApartmentshape_id());
			l.get(i).setApartmentShape(a);
		}
		return l;
	}
}
