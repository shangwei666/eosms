package com.huanying.sales.unit;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.huanying.sales.apartmentshape.ApartmentShape;

@Entity
public class Unit_Apartmentshape implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3827139707594600775L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int unit_id;
	private int apartmentshape_id;
	private String room_num;
	@Column(name="status",nullable=false,columnDefinition="INT default 1")
	private int status;			//状态    0删除   1正常
	@Transient
	private ApartmentShape apartmentShape;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUnit_id() {
		return unit_id;
	}
	public void setUnit_id(int unit_id) {
		this.unit_id = unit_id;
	}
	public int getApartmentshape_id() {
		return apartmentshape_id;
	}
	public void setApartmentshape_id(int apartmentshape_id) {
		this.apartmentshape_id = apartmentshape_id;
	}
	public String getRoom_num() {
		return room_num;
	}
	public void setRoom_num(String room_num) {
		this.room_num = room_num;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public ApartmentShape getApartmentShape() {
		return apartmentShape;
	}
	public void setApartmentShape(ApartmentShape apartmentShape) {
		this.apartmentShape = apartmentShape;
	}

}
